/* $Id: GSSort.h,v 1.60 2006/03/07 17:32:50 tl Exp $ */


/* misc */

#define LENEVTIME 900000
#define LENDATATIME 150000
#define NANG 17
#define EXTLEN 5000
#define STRLEN 132

#define GAMMASPHERE 0
#define LEN_CLEAN 1
#define LEN_DIRTY 2
#define LEN_BGO 3
#define LENCLEAN 4
#define LENDIRTY 5
#define LENTOTAL 6
#define SUMGE 7
#define SUMBGO 8
#define CLOCK1 9
#define CLOCK2 10
#define TAC2 11
#define LENEXT 12

#define NGSPEV 0
#define GSPEVL1 13
#define GSPEVL2 GSPEVL1+NGSPEV

#define NFERA 1000
#define MAX2DS 100

#define NOTDEF -1
#define FALSE 0
#define TRUE 1
#define PEVTYPE 1
#define GGMATTYPE 2
#define GAMPEV 3

/*----------------------------------*/
/* structures and associated arrays */
/*----------------------------------*/

#define NPEVMIN GSPEVL2+NGSPEV
#define MAXPEV 5000
#define MAXDEVMATRIX 50

typedef struct mpev
{
  int             type;
  int             pev1;
  double          factor;
  double          offset;
  int             pev2;
  int             cond;
}               MPEV;

int             nmpev = 0;
MPEV            mpev[MAXPEV];

/*----------------------------------------*/

typedef struct hit2dmat
{
  int             dlo1;
  int             dhi1;
  int             dlo2;
  int             dhi2;
  double          thresh;
  char            name[32];
  int             emaxpev;
}               HIT2DMAT;

int             nHit2DMat = 0;
HIT2DMAT        Hit2DMat[MAXDEVMATRIX];

/*----------------------------------------*/

typedef struct devmatrix
{
  int             dlo;
  int             dhi;
  char            name[32];
  int             nn;
  int             ylo;
  int             yhi;
}               DEVMATRIX;

int             nDevMatrix = 0;
DEVMATRIX       DevMatrix[MAXDEVMATRIX];

/*----------------------------------------*/

typedef struct ratespectrum
{
  char            name[32];
  int             len;
  double          lo;
  double          hi;
  int             pev;
  double          incrmt;
}               RATESPECTRUM;

int             nratesp = 0;
RATESPECTRUM    ratesp[NGE];

/*----------------------------------------*/

typedef struct pseudoev
{
  int             filled;
  double          val;
  char            name[32];
  int             vsn;
  int             ch;
  int             counting;
  int             thresh;
  int             calib;
  double          offset;
  double          gain;
  int             ratespno;
  int             EarlyGate;
  int             EarlyGate_pev;
  double          EarlyGate_lo;
  double          EarlyGate_hi;
}               PSEUDOEV;

int             LenPEv = NPEVMIN;
PSEUDOEV        PEv[MAXPEV];

/*----------------------------------------*/

typedef struct pevcond
{
  int             filled;
  int             type;
  char            name[32];
  char            fn[72];
  int             lot[L14BITS];
  char            strrange[256];
  /* double lo; */
  /* double hi; */
  int             pev1;
  int             pev2;
  int             ok;
  TCutG          *win;
  unsigned int    count;
}               PEVCOND;

int             NPEvCond = 0;
PEVCOND         PEvCond[MAXPEV];

/*----------------------------------------*/

typedef struct applycond
{
  int             pev;
  int             pevtype;
  int             cond;
  int             inherited;
}               APPLYCOND;

int             nApplyCond = 0;
APPLYCOND       ApplyCond[MAXPEV];

/*----------------------------------------*/

typedef struct pev1dbin
{
  int             pev;
  int             len;
  double          lo;
  double          hi;
}               PEV1DBIN;

int             NPEv1DBIN = 0;
PEV1DBIN        PB1D[MAXPEV];

/*----------------------------------------*/

typedef struct pev2dbin
{
  int             pevx;
  int             lenx;
  double          lox;
  double          hix;
  int             pevy;
  int             leny;
  double          loy;
  double          hiy;
}               PEV2DBIN;

int             NPEv2DBIN = 0;
PEV2DBIN        PB2D[MAXPEV];

/*----------------------------------------*/

typedef struct gamgam
{
  int             filled;	/* 1'th update control */
  char            name[32];
  int             len;
  double          lo;
  double          hi;
  int             prompt_cond;
  int             dtbggm_cond;
}               GAMGAM;

int             nGamxGam = 0;
GAMGAM          GG[MAX2DS];

/*----------------------------------------*/
/* fera update control update via pev */

typedef struct gampev2dbin
{
  char            name[32];
  int             pev;
  int             pevlen;
  double          pevlo;
  double          pevhi;
  int             gamlen;
  double          gamlo;
  double          gamhi;
  int             prompt_cond;
}               GAMPEV2DBIN;

int             NGamPEv2DBin = 0;
GAMPEV2DBIN     PEvG[NFERA];

/*----------------------------------------*/

typedef struct timemask
{
  char            name[32];
  int             lot[L14BITS];
  char            strrange[256];
}               TIMEMASK;

int             NTimeMasks = 0;
TIMEMASK        TMask[MAX2DS];



/*----------------------------------------*/
#include <iostream>
using namespace std;



//////////////////////////////////////////////////////////////////////////////////////////////////
// DSG_tree: simple class to store the Silicon detectors' data which then is passed to the TTree.
// The class is not saved in the root file.
//////////////////////////////////////////////////////////////////////////////////////////////////
class HitList {
 public:

  // Constructor
  HitList(int MaxHits) {
    this->MaxHits = MaxHits;
    NumHits = MaxHits;
    mod = new int[MaxHits];
    ch = new int[MaxHits];
    val = new int[MaxHits];
    
    MaxQSortIterations = int(MaxHits*log2(MaxHits));

  };

  // Reset function
  void Reset() 
  {
    int Limit = NumHits;
    if (NumHits>MaxHits)
      Limit = MaxHits;
    for (int i=0; i<Limit; i++) {
      mod[i] = -1;
      ch[i] = -1;
      val[i] = -1;
    }
    NumHits = 0;
    return;
  };

  // Sort the hits data in descending order
  void SortHits() {
    QSortCounter = 0;
    QuickSort(0, NumHits-1); 
    return;
  }

  // Members
  int* mod;
  int* ch;
  int* val;
  int NumHits;

 private:
  int MaxHits;
  int MaxQSortIterations;
  int QSortCounter;
  
  ////////////////////////////////////////////////////////////////////////////////////
  // Quicksort algorithm modified to get the largest values first.  Source:
  // http://www.algolist.net/Algorithms/Sorting/Quicksort
  ////////////////////////////////////////////////////////////////////////////////////
  void QuickSort(int left, int right) 
  {
    int i = left, j = right;
    int tmp;
    int pivot = val[(int)((left + right)/2)];  

    QSortCounter++;
    if (QSortCounter>MaxQSortIterations) {
      cout << "Warning: QuickSort reached maximum number of iterations (" 
	   << MaxQSortIterations << ")." << endl;
      return;
    }

    // Partition 
    while (i <= j) {
      while (val[i] > pivot)
	i++;
      while (val[j] < pivot)
	j--;
      if (i <= j) {
	// Change value
	tmp = val[i];
	val[i] = val[j];
	val[j] = tmp;
	// Change module
	tmp = mod[i];
	mod[i] = mod[j];
	mod[j] = tmp;
	// Change channel
	tmp = ch[i];
	ch[i] = ch[j];
	ch[j] = tmp;

	i++;
	j--;
      }
    };
  
    // Recursion
    if (left < j)
      QuickSort(left, j);
    if (i < right)
      QuickSort(i, right);

    return;
  }

};


//////////////////////////////////////////////////////////////////////////////////////////////////
// DSG_tree: simple class to store the Germanium detectors' data which then is passed to the TTree.
// The class is not saved in the root file.
//////////////////////////////////////////////////////////////////////////////////////////////////
class GSHitList {
 public:

  // Constructor
  GSHitList(int MaxHits) {
    this->MaxHits = MaxHits;
    NumHits = MaxHits;
    id = new int[MaxHits];
    ge = new float[MaxHits];
    gth = new float[MaxHits];
    gt = new float[MaxHits];
  };
  
  // Reset function
  void Reset() 
  {
    int Limit = NumHits;
    if (NumHits>MaxHits)
      Limit = MaxHits;
    for (int i=0; i<Limit; i++) {
      id[i] = -1;
      ge[i] = -1.0;
      gth[i] = -1000.0;
      gt[i] = 0.0;
    }
    NumHits = 0;
    return;
  };

  // Members
  int* id;
  float* ge;
  float* gth;
  float* gt;
  int NumHits;
  int MaxHits;
};
