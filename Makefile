
# $Id: Makefile,v 1.709 2007/10/04 21:18:37 tl Exp $

# definitions
#-------------------------------------------------

# source bin (output dir) and destination (install dir)

BIN    = bin
SRC    = .

# include the platform spcific options

#PLTFMOPT=Makefile.$(shell uname).$(shell uname -r)
PLTFMOPT=Makefile.$(shell uname)
include $(PLTFMOPT)

# lock sources when they are checked out
# ... great for me but gives trouble for other users

#COFLAGS = -l

help:	
	echo "no default build, type make <utility>"

#---------------------------------------------------------
# tmp

cmd:		cmd.c
		$(cc) $^ -o $(BIN)/$@
		cmd sendto con5

#-------------------------------------------------

install:	
		ls -l ../bin/*
		cp -i ../bin/* /dk/bgo2/util/bin/`uname``uname -r`
		rm -i ../bin/*

#-------------------------------------------------
# install GSSort/GSUtil on web server

wwwinstall:	
		/home2/tl/scripts/wwwinstallgssort

#-------------------------------------------------
# labjack

u3.o:		u3.c u3.h
		$(cc) -c $^

labjack_temp:	labjack_temp.c u3.o
		$(cc) -o $(BIN)/$@ $^

#-------------------------------------------------
# alarms

ht_alarm:	ht_alarm.c
		$(cc) -o $(BIN)/$@ $^
#		$(BIN)/$@ 73.1 54.3 67.6 63.3

#---------------------------------------------------------
# RPC developments

rpctest_rusers:	$(SRC)/rpctest_rusers.c
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ \
		-lrpcsvc -lnsl

#---------------------------------------------------------
# ROOT stuff

tst:		$(SRC)/tst.c GetSwDef.o
		$(cc) $(CCFLAG) $^ -o $@
		tst

sigtest:	$(SRC)/sigtest.c
		$(cc) $(SRC)/sigtest.c -o $(BIN)/sigtest
		sigtest

GSUtil_cc.so:	$(SRC)/GSUtil.cc spe_fun.c 2d_fun.c

GSSort.o:	$(SRC)/GSSort.cxx GSSort.h GSII_angles.h \
		UserDeclare.h UserInit.h UserRawEv.h UserEv.h \
		UserExit.h UserFunctions.h UserInclude.h UserPreCond.h \
		GSSort_global_declares.h \
		GSSort_root_spectra.h GSPrintEvent.h UserChat.h \
		UserStat.h UserGoodEv.h
		$(CC) $(CCFLAG) $(ROOTINC) -c $(SRC)/GSSort.cxx


GSSortOBJ =     gsII_bufpCC.o \
		get_gsII_evCC.o \
		get_disk_evCC.o \
		get_net_evCC.o \
		tape_funCC.o \
		gsII_utilsCC.o \
		time_stampCC.o \
		get_a_seedCC.o \
		GSudpGetBufCC.o \
		GSudpUtil_rCC.o \
		RdOffFileCC.o \
		RdGeCalFileCC.o \
		GSSort.o \
		str_decompCC.o \
		spe_funCC.o\
		/home/dasago/Dropbox/Codes/Detectors/CySD/CySD.o

GSSort:		$(GSSortOBJ) 
		$(CC) $(GSSortOBJ) $(ROOTLIB) \
		-o $(BIN)/GSSort
#		$(BIN)/GSSort -input disk data.dat RECREATE		

addrootfiles:	$(SRC)/addrootfiles.cxx
		$(CC)   $(ROOTLIB) $(ROOTINC) $(NSL) $(SOCKET) \
		$(SRC)/addrootfiles.cxx -o $(BIN)/addrootfiles
		addrootfiles gs1.root gs1.root sum.root

#--

Event:		MainEvent.o libEvent.so
		$(CC) -O MainEvent.o libEvent.so -L/usr/local/root/lib \
		-lCore -lCint -lHist -lGraf -lGraf3d -lGpad -lTree \
		-lRint -lPostscript -lMatrix -lPhysics -lm -ldl $(NSL) $(SOCKET) \
		-o $(BIN)/Event

libEvent.so:	Event.o EventDict.o
		$(CC) -G -O Event.o EventDict.o -o libEvent.so

Event.o:	$(SRC)/Event.cxx Event.h
		$(CC) -O -KPIC -I/usr/local/root/include  -c $(SRC)/Event.cxx 

EventDict.o:	$(SRC)/EventDict.cxx EventDict.h
		$(CC) -O -KPIC -I/usr/local/root/include  -c $(SRC)/EventDict.cxx 
		$(CC) -G -O Event.o EventDict.o -o  libEvent.so

MainEvent.o:	$(SRC)/MainEvent.cxx Event.h
		$(CC) -O -KPIC -I/usr/local/root/include  -c $(SRC)/MainEvent.cxx 

#--

hsimple:	hsimple.o
		$(CC) -O hsimple.o -L/usr/local/root/lib \
		-lCore -lCint -lHist -lGraf -lGraf3d -lGpad -lTree \
		-lRint -lPostscript -lMatrix -lPhysics -lm -ldl \
		$(NSL) $(SOCKET)  -o hsimple.so

hsimple.o:	$(SRC)/hsimple.cxx
		CC -O -KPIC -I/usr/local/root/include -c hsimple.cxx

hprod:		$(SRC)/hprod.C
		$(CC) $(SRC)/hprod.C -I/usr/local/root/include \
		-lCore -lCint -lHist -lGraf -lGraf3d -lGpad -lTree \
		-lRint -lPostscript -lMatrix -lPhysics -lm -ldl \
		$(NSL) $(SOCKET)  -o hprod
		hprod

#---------------------------------------------------------
# Solaris 5.8 network sender and receivers
#

GSudpSender:	$(SRC)/GSudpSender.c $(SRC)/efftape.h  $(SRC)/sender.h \
		GSudpUtil_sCC.o
		$(CC) -o $(BIN)/GSudpSender $(SRC)/GSudpSender.c \
		GSudpUtil_sCC.o $(SOCKET)  $(NSL)
#		GSudpSender -tohost gam0 -from /dev/rmt/1mbn

GSudpReceiver:	gsII_bufpCC.o \
		get_gsII_evCC.o \
		get_disk_evCC.o \
		get_net_evCC.o \
		tape_funCC.o \
		gsII_utilsCC.o \
		time_stampCC.o \
		get_a_seedCC.o \
		GSudpGetBufCC.o \
		GSudpUtil_rCC.o \
		GSudpReceiver.o \
		str_decompCC.o
		$(CC) $(CCLAGS)  $^ -o $(BIN)/$@ $(NSL) $(SOCKET)
#		GSudpReceiver -o /dev/null -n_buffers 200

GSudpReceiver.o:	$(SRC)/GSudpReceiver.c \
			$(SRC)/efftape.h  $(SRC)/sender.h \
			$(SRC)/GSudpReceiver.h  $(SRC)/gsII.h 
			$(CC) $(CCFLAG) -DTAPE  -c $(SRC)/GSudpReceiver.c

GSudpGetBufCC.o:	$(SRC)/GSudpGetBuf.c\
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(CC) $(CCFLAG) -c $(SRC)/GSudpGetBuf.c -o GSudpGetBufCC.o

GSudpGetBuf.o:	$(SRC)/GSudpGetBuf.c\
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(cc) $(CCFLAG) -c $(SRC)/GSudpGetBuf.c -o GSudpGetBuf.o


GSudpUtil_r.o:	$(SRC)/GSudpUtil_r.c
		$(cc) $(CCFLAG) -c $^ -o $@

GSudpUtil_s.o:	$(SRC)/GSudpUtil_s.c
		$(cc) $(CCFLAG) -c $^ -o $@

GSudpUtil_rCC.o:	$(SRC)/GSudpUtil_r.c
		$(CC) $(CCFLAG) -c $^ -o $@

GSudpUtil_sCC.o:	$(SRC)/GSudpUtil_s.c
		$(CC) $(CCFLAG) -c $^ -o $@

#---------------------------------------------------------
# MSU tape read/sort   tl-95
#

# simple tape dump/binner

KL_CM_GEN= $(SRC)/dmp_msu.c time_stamp.o tape_fun.o spe_fun.o 

dmp_msu:	$(KL_CM_GEN) $(SRC)/msu.h msu_8.o msu_9.o
		$(cc) $(CCFLAG) -o $(BIN)/dmp_msu_8 \
		$(KL_CM_GEN) $(SRC)/msu_8.o
		$(cc) $(CCFLAG) -o $(BIN)/dmp_msu_9 \
		$(KL_CM_GEN) $(SRC)/msu_9.o
 
BGO_1= $(SRC)/dmp_msu_bgo.c time_stamp.o tape_fun.o spe_fun.o 

dmp_msu_bgo:	$(BGO_1) $(SRC)/msu.h msu_8.o msu_9.o bgo_ev.o
		$(cc) $(CCFLAG) -o $(BIN)/dmp_msu_bgo_8 \
		$(BGO_1) $(SRC)/msu_8.o bgo_ev.o
		$(cc) $(CCFLAG) -o $(BIN)/dmp_msu_bgo_9 \
		$(BGO_1) $(SRC)/msu_9.o bgo_ev.o

BGO_2= $(SRC)/bgo_sort.c time_stamp.o tape_fun.o spe_fun.o str_decomp.o 

bgo_sort:	$(BGO_2) $(SRC)/msu.h msu_8.o msu_9.o bgo_ev.o
		$(cc) $(CCFLAG) -o $(BIN)/bgo_sort_8 \
		$(BGO_2) $(SRC)/msu_8.o bgo_ev.o -lm
		$(cc) $(CCFLAG) -o $(BIN)/bgo_sort_9 \
		$(BGO_2) $(SRC)/msu_9.o bgo_ev.o -lm

msu_8.o:	$(SRC)/msu.c $(SRC)/msu.h
		$(cc) -c $(CCFLAG) -o msu_8.o\
		-DINTAPE=8 $(SRC)/msu.c

msu_9.o:	$(SRC)/msu.c $(SRC)/msu.h
		$(cc) -c $(CCFLAG) -o msu_9.o\
		-DINTAPE=9 $(SRC)/msu.c	
#---------------------------------------------------------
# incubator software
#
INCUB_ANL=	incub_anl.o     \
		incub_usersub.o \
		incub_anl_c.o   \
		pass_to_tape.o  \
		get_skim_ev_tape.o   \
		get_skim_ev_disk.o   \
		time_stamp.o    \
                info.o \
		incub_c_user.o

INCUB_ANL_H=	$(SRC)/incub8r.h \
		$(SRC)/incub8r.par \
		$(SRC)/info.h \
		$(SRC)/rebel.h

incub_anl:		$(INCUB_ANL_H) $(INCUB_ANL) 
			$(FF) -o $(BIN)/incub_anl_tl $(INCUB_ANL)

incub_anl.o:		$(SRC)/incub_anl.f $(SRC)/incub8r.par
			$(FF) -c $(SRC)/incub_anl.f
incub_usersub.o:	$(SRC)/incub_usersub.f
			$(FF) -c $(SRC)/incub_usersub.f
incub_anl_c.o:		$(SRC)/incub_anl_c.c
			$(cc) -c $(CCFLAG) $(SRC)/incub_anl_c.c
pass_to_tape.o:		$(SRC)/pass_to_tape.c
			$(cc) -c $(CCFLAG) $(SRC)/pass_to_tape.c
incub_c_user.o:		$(SRC)/incub_c_user.c
			$(cc) -c $(CCFLAG) $(SRC)/incub_c_user.c

#---------------------------------------------------------
# misc utilities

xlines:		$(SRC)/xlines.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@ test.txt 1 10 0


einventory:	$(SRC)/einventory.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@

uc3906_aa:	$(SRC)/uc3906_aa.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@

geeff60co:	$(SRC)/geeff60co.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@

uc3906_mf:	$(SRC)/uc3906_mf.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@

usecClockDiff.o:	$(SRC)/usecClockDiff.c
		$(cc) $(CCFLAG) -c $(SRC)/$^

test_udc:	$(SRC)/test_udc.c usecClockDiff.o TSorderOK.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^
#		$(BIN)/$@ 10 10    32767 10 11 0
		$(BIN)/$@ 10 16383 32767 11  0 0

TSorderOK.o:	$(SRC)/TSorderOK.c 
		$(cc) $(CCFLAG) -c $(SRC)/$^			

xm:		$(SRC)/xm.c 
		$(cc) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^ 
#		$(BIN)/$@ testdata /home2/tl/d6/n.dat 111 >! x.dat
#		$(BIN)/$@ x.dat    /home2/tl/d6/n.dat 111

printBinaryI16:	$(SRC)/printBinaryI16.c tlutil.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^ 
#		$(BIN)/$@ 47 23

gsexportfs:	$(SRC)/gsexportfs.c
		$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^

gsunexportfs:	$(SRC)/gsunexportfs.c
		$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^

tt:		$(SRC)/tt.c
		$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^
		$(BIN)/$@ 10

ascii_ct_xy:	$(SRC)/ascii_ct_xy.c
		$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^
		$(BIN)/$@ x.xy y.xy 2

ascii_ct_xydy:	$(SRC)/ascii_ct_xydy.c
		$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^
		$(BIN)/$@ x.xydy y.xydy

FindBgoSectorECal:	$(SRC)/FindBgoSectorECal.c
			$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^
			$(BIN)/$@ ecal.data ecal.cal  185.92 382.559

FindBgoPeaks:		$(SRC)/FindBgoPeaks.c spe_fun.o tlutil.o
			$(cc) $(CCFLAG) -o $(BIN)/$@ $^
			$(BIN)/$@ rawbgo

mkbgocmd:		$(SRC)/mkbgocmd.c
			$(cc) $(CCFLAG) -o $(BIN)/$@ $^
			$(BIN)/$@					

error_prop:		$(SRC)/error_prop.c
			$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^

ecal:			$(SRC)/ecal.c
			$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^

mk_entry_dist:		$(SRC)/mk_entry_dist.c
			$(CC) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^
			rm -f  ei.mat

calc_a2a4:		$(SRC)/calc_a2a4.F ak.o uk.o rhok.o \
			bk.o fk.o fun6j.o fun3j.o upcase.o my_exp.o fac.o
			$(FF) -o $(BIN)/calc_a2a4 \
			$(SRC)/calc_a2a4.F ak.o uk.o rhok.o \
			bk.o fk.o fun6j.o fun3j.o upcase.o my_exp.o fac.o

weisskopf:		$(SRC)/weisskopf.F fun3j.o fac.o
			$(FF) -o $(BIN)/weisskopf \
			$(SRC)/weisskopf.F fun3j.o fac.o 


fusion_prod:		elmsym.o $(SRC)/fusion_prod.f elmsym.o
			$(FF) -o $(BIN)/fusion_prod \
			elmsym.o $(SRC)/fusion_prod.f

elmsym.o:		$(SRC)/elmsym.f
			$(FF) -c $(SRC)/elmsym.f

coul:			$(SRC)/coul.f
			$(FF) -o $(BIN)/coul coul.f

pad_calc_3j:		$(SRC)/pad_calc_3j.F fun3j.o fac.o
			$(FF) -o $(BIN)/pad_calc_3j \
			$(SRC)/pad_calc_3j.F fun3j.o fac.o

se:			$(SRC)/se.F
			$(FF) -o $(BIN)/se $(SRC)/se.F

trans_quad:		$(SRC)/trans_quad.F fun3j.o upcase.o fac.o
			$(FF) -o $(BIN)/trans_quad \
			$(SRC)/trans_quad.F fun3j.o upcase.o fac.o


#---------------------------------------------------------
# palmpilot utilities

get_cmpl_todos:	$(SRC)/get_cmpl_todos.c
		gcc -o $(BIN)/get_cmpl_todos \
		-I/usr/local/include \
		-L/usr/local/lib \
		get_cmpl_todos.c -lpisock $(SOCKET)

#---------------------------------------------------------
# kl_sd/kla sources

KL_CM_GEN =	get_a_seed.o \
		top_rwrite.o \
		ran_gauss.o  \
		goe_level_sel.o \
		write_matrix.o \
		write_vector.o

kl_cm2_gen:	$(SRC)/kl_cm2_gen.F $(KL_CM_GEN)
		$(FF) -o $(BIN)/kl_cm2_gen \
		$(SRC)/kl_cm2_gen.F $(KL_CM_GEN) \
		$(TRAILOR) -L /usr/local/cern2000/lib -l mathlib

goe_level_sel:	$(SRC)/goe_level_sel.F get_a_seed.o top_rwrite.o \
		rbin.o ran_gauss.o write_matrix.o write_vector.o
		$(FF) -o $(BIN)/goe_level_sel \
		-DPROG $(SRC)/goe_level_sel.F get_a_seed.o top_rwrite.o \
		rbin.o ran_gauss.o  write_matrix.o write_vector.o \
		$(TRAILOR) -L /cern/lib -l mathlib
		goe_level_sel

#--------------------------------------------------------

kl_sd.o:	$(SRC)/kl_sd.F kl.h
		$(FF) -c $(SRC)/kl_sd.F

kl_e1_gen.o:	$(SRC)/kl_e1_gen.F kl.h
		$(FF) -c $(SRC)/kl_e1_gen.F

kl_e2_gen.o:	$(SRC)/kl_e2_gen.F kl.h
		$(FF) -c $(SRC)/kl_e2_gen.F	

kl_prob_get.o:	$(SRC)/kl_prob_get.F
		$(FF) -c $^	

kl_sd_decay.o:	$(SRC)/kl_sd_decay.F kl.h
		$(FF) -c $(SRC)/kl_sd_decay.F	


kl_sd:		kl_sd.o kl_sel_out.o kl_init_well_sel.o kl_sd_decay.o \
		rho_sd_n.o smooth_2d.o io_rec.o\
		kl_rho.o kl_make_array.o elmsym.o kl_entry_exp.o upcase.o \
		kl_sd_t_coeff.o top_rwrite.o my_exp.o get_a_seed.o \
		kl_width_store.o sm3.o ran_gauss.o kl_e1_gen.o kl_e2_gen.o \
		cleb.o kl_cm2_nout.o array_interpolate.o hs2_io.o \
		kl_prob_get.o  read_rotd_mat.o 
		$(FF) -o $(BIN)/$@ $^ \
		$(TRAILOR) -L /usr/local/cern2000/lib -lmathlib 

#--------------------------------------------------------

kla.o:	$(SRC)/kla.F kla_list.h kla.h 
	$(FF) -c $(SRC)/kla.F

kla:	rms.o top_rwrite.o kla_norm_array.o kla_norm.o ibin.o \
	kla_ridge.o top_iwrite.o kla_hs2.o ims.o w_theta.o rbin.o \
	v_over_c.o upcase.o array_interpolate.o hs2_io.o io_rec.o \
	kla.o spe_fun.o my_exp.o 2d_fun.o kla_discrete.o \
	ran_gauss.o get_a_seed.o
	$(FF) -o $(BIN)/$@ $^ -L /usr/local/cern2000/lib -l mathlib

#--------------------------------------------------------
# standalone programs

mm:		mm.o mm_do.o fi2.o list_val.o
		$(CC) $(CCFLAG) -o $(BIN)/$@ $^

mm.o:		mm.c
		$(CC) $(CCFLAG) -c $^

mm.c:		mm.h					

mm_do.o:	mm_do.c
		$(CC) $(CCFLAG) -c $^			

fi2.o:		fi2.c
		$(CC) $(CCFLAG) -c $^			

list_val.o:	list_val.c
		$(CC) $(CCFLAG) -c $^			

KL_SD_DECAY =  kl_sd_t_coeff.o \
	my_exp.o \
        kl_width_store.o \
        get_a_seed.o \
        kl_e1_gen.o \
        kl_e2_gen.o \
        cleb.o \
        kl_cm2_nout.o \
        kl_rho.o \
        kl_prob_get.o \
        upcase.o \
        sm3.o \
        array_interpolate.o

kl_sd_decay:	$(SRC)/kl_sd_decay.F $(KL_SD_DECAY) 
		$(FF) -Dprog -o $(BIN)/kl_sd_decay.F $(KL_SD_DECAY) \
		$(TRAILOR)

#-------------------------------------------------------

kl_cm2_nout:	$(SRC)/kl_cm2_nout.F get_a_seed.o
		$(FF) -o $(BIN)/kl_cm2_nout -DPROG kl_cm2_nout.F \
		get_a_seed.o


xkl_new_nout:	xkl_new_nout.F kl_new_nout.o get_a_seed.o goe_level_sel.o \
		write_matrix.o ran_gauss.o write_vector.o kl_rho.o \
		sm3.o my_exp.o array_interpolate.o kl_sd_t_coeff.o \
		kl_e1_gen.o kl_e2_gen.o cleb.o
		$(FF) -o $(BIN)/$@  $^ -L \
		/usr/local/cern2000/lib -l mathlib

kl_new_nout.o:	$(SRC)/kl_new_nout.F 
		$(FF) -c  $^ 				

kl_entry_exp:	$(SRC)/kl_entry_exp.F get_a_seed.o smooth_2d.o
		$(FF) -o $(BIN)/kl_entry_exp -DPROG kl_entry_exp.F \
		get_a_seed.o smooth_2d.o
		kl_entry_exp

#--------------------------------------------------------

kla_ridge.o:	$(SRC)/kla_ridge.c kla.h
		$(cc) $(CCFLAG) -c $^

kla_discrete.o:	$(SRC)/kla_discrete.c kla.h
		$(cc) $(CCFLAG) -c $^


gen_M1E2_comp:	$(SRC)/gen_M1E2_comp.c get_a_seed.o
		$(cc)  $(CCFLAG) -o $(BIN)/$@ $^ -lm
		$(BIN)/$@

#---------------------------------------------------------
# GS-II tape read/sort
#

# simple tape dump/binner

dmp_gsub:	$(SRC)/gsII.h $(SRC)/gsub.h \
		$(SRC)/dmp_gsub.c time_stamp.o tape_fun.o  \
		gsub_bufp.o get_gsub_ev.o spe_fun.o wr_2d_his.o \
		rd_damm_ban.o 
		$(cc) $(CCFLAG) -o $(BIN)/dmp_gsub \
		$(SRC)/dmp_gsub.c time_stamp.o tape_fun.o \
		gsub_bufp.o get_gsub_ev.o spe_fun.o wr_2d_his.o \
		rd_damm_ban.o 
#		dmp_gsub 100 10


dmp_gsII.o:	$(SRC)/dmp_gsII.c \
		$(SRC)/gsII.h $(SRC)/GSudpReceiver.h $(SRC)/efftape.h 
		$(cc) $(CCFLAG) -c $(SRC)/dmp_gsII.c

dmp_gsII:	dmp_gsII.o time_stamp.o spe_fun.o tape_fun.o \
		get_disk_ev.o get_gsII_ev.o get_net_ev.o gsII_utils.o \
		gsII_bufp.o str_decomp.o GSudpUtil_r.o GSudpGetBuf.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ $(NSL) $(SOCKET)  

fera_test:	$(SRC)/fera_test.c time_stamp.o tape_fun.o \
		gsII_bufp.o get_gsII_ev.o spe_fun.o 2d_fun.o \
		$(SRC)/gsII.h $(SRC)/GSudpReceiver.h $(SRC)/efftape.h \
		gsII_utils.o
		$(cc) $(CCFLAG) -o $(BIN)/fera_test \
		$(SRC)/fera_test.c time_stamp.o tape_fun.o \
		gsII_bufp.o get_gsII_ev.o spe_fun.o gsII_utils.o \
		2d_fun.o
		fera_test -n 500 -p 300 -input /dev/rmt/0mbn;\
		trew  /dev/rmt/0mbn 0 &


mk_hk_rsp:	mk_hk_rsp.o \
		time_stamp.o \
		tape_fun.o \
		gsII_bufp.o \
		get_gsII_ev.o \
		get_disk_ev.o \
		get_net_ev.o \
		GSudpGetBuf.o \
		GSudpUtil_r.o \
		spe_fun.o \
		gsII_utils.o \
		str_decomp.o \
		get_a_seed.o \
		BgoCal.o \
		rdTimeOffsetFile.o 
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ $(NSL) $(SOCKET)
#		mk_hk_rsp \
#		  -input /dev/rmt/0mbn \
#		  -tge 5435 5625 \
#                 -tbgo 2445 2765 \
#		  -gate 5497 5520 \
#		  -line_e 898.0 \
#		  -gecal 0 0.3333333 \
#		  -bgocal 0 2.5 \
#                 -n 10000
#		trew  /dev/rmt/0mbn 0 &

mk_hk_rsp.o:	$(SRC)/mk_hk_rsp.c gsII.h skim.h
		$(cc) $(CCFLAG) -c $(SRC)/mk_hk_rsp.c		

hk_unfold:	$(SRC)/hk_unfold.c time_stamp.o mat_analyze.o 2d_fun.o\
		spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/hk_unfold \
		$(SRC)/hk_unfold.c time_stamp.o mat_analyze.o \
		2d_fun.o spe_fun.o -lm
#		hk_unfold -k_rsp gsfma30_k_rsp_true.mascii \
#		-h_rsp gsfma30_h_rsp_true.mascii  \
#		-hk hk.mascii -mhk 1 -ite 20 -ns 1

hk_fold:	$(SRC)/hk_fold.c time_stamp.o mat_analyze.o 2d_fun.o \
		spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/hk_fold spe_fun.o \
		$(SRC)/hk_fold.c time_stamp.o mat_analyze.o \
		2d_fun.o -lm
#		hk_fold -k_rsp k_rsp_true.mascii -h_rsp h_rsp_true.mascii \
#		-in hk.mascii -out hk_folded.mascii

mk_hk_mat:	$(SRC)/mk_hk_mat.c
		$(cc) $(CCFLAG) -o $(BIN)/mk_hk_mat \
		$(SRC)/mk_hk_mat.c -lm
		mk_hk_mat 100000

dmp_net:	$(SRC)/dmp_gsII.c \
		time_stamp.o \
		tape_fun.o \
		gsII_bufp.o \
		gsII_utils.o \
		get_net_ev.o \
		spe_fun.o \
		GSudpGetBufCC.o \
		$(SRC)/gsII.h \
		$(SRC)/GSudpReceiver.h \
		$(SRC)/efftape.h \
		$(SRC)/sender.h \
		$(SRC)/GSudpReceiver.h
		$(cc) $(CCFLAG) -o $(BIN)/dmp_net -DNET \
		$(SRC)/dmp_gsII.c \
		time_stamp.o \
		tape_fun.o \
		gsII_bufp.o \
		gsII_utils.o \
		get_net_ev.o \
		spe_fun.o \
		GSudpGetBufCC.o
		dmp_net 100 10 2000 1

gs_sort_bgo:	time_stamp.o \
		tape_fun.o \
		gsII_bufp.o \
		get_net_ev.o \
		spe_fun.o \
		GSudpGetBuf.o \
		gsII_utils.o \
		get_gsII_ev.o\
		get_disk_ev.o \
		str_decomp.o \
		GSudpUtil_r.o \
		gs_sort_bgo.o \
		BgoCal.o \
		get_a_seed.o 
		$(cc) $(CCFLAG) -o $(BIN)/$@ -DNET $^ $(NSL) $(SOCKET)
		gs_sort_bgo -input disk /mnt/bgo/tl/t300a_f8.data \
		-calfile ecal.cal -x all
#		gs_sort_bgo -x ODD -input /dev/rmt/1mbn&     


gs_sort_bgo.o:	$(SRC)/gs_sort_bgo.c efftape.h sender.h GSudpReceiver.h gsII.h
		$(cc) $(CCFLAG) -c $(SRC)/gs_sort_bgo.c

source_sort.o:	$(SRC)/source_sort.c gsII.h GSudpReceiver.h
		$(cc) $(CCFLAG) -c $(SRC)/source_sort.c

source_sort:	source_sort.o time_stamp.o tape_fun.o \
		gsII_bufp.o get_gsII_ev.o spe_fun.o str_decomp.o\
		gsII_utils.o 2d_fun.o get_disk_ev.o get_net_ev.o \
		GSudpGetBuf.o GSudpUtil_r.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ $(NSL) $(SOCKET)

source_sort1.o:	$(SRC)/source_sort1.c gsII.h GSudpReceiver.h
		$(cc) $(CCFLAG) -c $(SRC)/source_sort1.c

source_sort1:	source_sort1.o time_stamp.o tape_fun.o \
		gsII_bufp.o get_gsII_ev.o spe_fun.o str_decomp.o\
		gsII_utils.o 2d_fun.o get_disk_ev.o get_net_ev.o \
		GSudpGetBuf.o GSudpUtil_r.o get_a_seed.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ $(NSL) $(SOCKET)

tfix:		$(SRC)/tfix.c time_stamp.o tape_fun.o \
		get_skim_ev.o  spe_fun.o \
		$(SRC)/gsII.h
		$(cc) $(CCFLAG) -o $(BIN)/tfix \
		$(SRC)/tfix.c time_stamp.o tape_fun.o \
		get_skim_ev.o spe_fun.o

RdOffFileCC.o:	$(SRC)/RdOffFile.c
		$(CC) -c  $(CCFLAG) $^ -o $@

RdGeCalFileCC.o:	$(SRC)/RdGeCalFile.c
		$(CC) -c  $(CCFLAG) $^ -o $@

RdOffFile.o:	$(SRC)/RdOffFile.c
		$(cc) -c $(CCFLAG) $^

create_isomerdata.o:	create_isomerdata.c
		$(cc) -c $(CCFLAG) $^		

gsII_utils.o: 	$(SRC)/gsII_utils.c $(SRC)/efftape.h $(SRC)/gsII.h
		$(cc) -c $(CCFLAG) $(SRC)/gsII_utils.c

gsII_utilsCC.o: $(SRC)/gsII_utils.c $(SRC)/efftape.h $(SRC)/gsII.h
		$(CC) -c $(CCFLAG) -o gsII_utilsCC.o $(SRC)/gsII_utils.c

get_gsII_ev.o:	$(SRC)/get_gsII_ev.c $(SRC)/gsII.h $(SRC)/efftape.h\
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(cc) -c $(CCFLAG) -DTAPE $(SRC)/get_gsII_ev.c

get_gsII_evCC.o:	$(SRC)/get_gsII_ev.c $(SRC)/gsII.h $(SRC)/efftape.h\
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(CC) -c $(CCFLAG) -DTAPE -o get_gsII_evCC.o \
		$(SRC)/get_gsII_ev.c

get_disk_ev.o:	$(SRC)/get_gsII_ev.c $(SRC)/gsII.h $(SRC)/efftape.h\
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(cc) -c $(CCFLAG) -DDISK $(SRC)/get_gsII_ev.c \
		-o get_disk_ev.o

get_disk_evCC.o:	$(SRC)/get_gsII_ev.c $(SRC)/gsII.h $(SRC)/efftape.h \
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(CC) -c $(CCFLAG) -DDISK $(SRC)/get_gsII_ev.c \
		-o get_disk_evCC.o

get_net_ev.o:	$(SRC)/get_gsII_ev.c $(SRC)/gsII.h $(SRC)/efftape.h \
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h
		$(cc) -c $(CCFLAG) -DNET $(SRC)/get_gsII_ev.c \
		-o get_net_ev.o

get_net_evCC.o:	$(SRC)/get_gsII_ev.c $(SRC)/gsII.h $(SRC)/efftape.h\
		$(SRC)/sender.h $(SRC)/GSudpReceiver.h 
		$(CC) -c $(CCFLAG) -DNET $(SRC)/get_gsII_ev.c \
		-o get_net_evCC.o

get_gsub_ev.o:	$(SRC)/get_gsub_ev.c $(SRC)/gsII.h
		$(cc) -c $(CCFLAG) $(SRC)/get_gsub_ev.c

# buffer data handler, specific to GSII data format 

gsII_bufp.o:	$(SRC)/gsII_bufp.c $(SRC)/gsII.h \
		$(SRC)/efftape.h $(SRC)/GSudpReceiver.h
		$(cc) -c $(CCFLAG) $(ROOTINC) $(SRC)/gsII_bufp.c

gsII_bufpCC.o:	$(SRC)/gsII_bufp.c $(SRC)/gsII.h \
		$(SRC)/efftape.h $(SRC)/GSudpReceiver.h
		$(CC) -c $(CCFLAG) $(ROOTINC) -o gsII_bufpCC.o $(SRC)/gsII_bufp.c

gsub_bufp.o:	$(SRC)/gsub_bufp.c $(SRC)/gsII.h \
		$(SRC)/efftape.h $(SRC)/GSudpReceiver.h
		$(cc) -c $(CCFLAG) -o gsub_bufp.o $(SRC)/gsub_bufp.c

# other utilities

BgoCal.o:	$(SRC)/BgoCal.c  gsII.h
		$(cc)  -c $(CCFLAG) $^

time_stamp.o:	$(SRC)/time_stamp.c
		$(cc)  -c $(CCFLAG) $(SRC)/time_stamp.c

time_stampCC.o:	$(SRC)/time_stamp.c
		$(CC)  -c $(CCFLAG) -o time_stampCC.o $(SRC)/time_stamp.c  

spe_fun.o:	$(SRC)/spe_fun.c 
		$(cc) -c $(CCFLAG) $(SRC)/spe_fun.c

spe_funCC.o:	$(SRC)/spe_fun.c 
		$(CC) -c $(CCFLAG) -o spe_funCC.o $(SRC)/spe_fun.c

tape_fun.o:	$(SRC)/tape_fun.c
		$(cc)  -c $(CCFLAG) $(SRC)/tape_fun.c

tape_funCC.o:	$(SRC)/tape_fun.c
		$(CC)  -c $(CCFLAG) -o tape_funCC.o $(SRC)/tape_fun.c

make_lookup:	$(SRC)/make_lookup.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/make_lookup  \
		spe_fun.o $(SRC)/make_lookup.c
#		make_lookup de_vs_e.dat test.dat

exp_warning:	$(SRC)/exp_warning.c
		$(cc)  -o $(BIN)/exp_warning \
		$(SRC)/exp_warning.c
		exp_warning 11526 tl


wr_mailbox.o:	$(SRC)/wr_mailbox.f
		$(FF) -c $(SRC)/wr_mailbox.f

mk_mailbox.o:	$(SRC)/mk_mailbox.f
		$(FF) -c $(SRC)/mk_mailbox.f

GetSwDef.o:	$(SRC)/GetSwDef.c
		$(cc) $(CCFLAG) -c $^

#---------------------------------------------------------
# UDP sender/receiver example from sun WWW pages

udp_s_example:	$(SRC)/udp_s_example.c GSudpUtil_sCC.o
		$(CC) $(CCLAGS)  -o $(BIN)/udp_s_example \
		$(SRC)/udp_s_example.c GSudpUtil_sCC.o $(SOCKET) $(NSL)
#		udp_s_example gam5 1101 "message from `hostname` on `date`"

udp_r_example:	$(SRC)/udp_r_example.c GSudpUtil_rCC.o
		$(CC) $(CCLAGS)  -o $(BIN)/udp_r_example \
		$(SRC)/udp_r_example.c  GSudpUtil_rCC.o $(SOCKET) $(NSL)
#		udp_r_example 1101


#---------------------------------------------------------
# 2D plotting routines
#

pgmdemo:	pgmdemo.c
		gcc -O -c -I$(PGPLOT_DIR) -I/usr/dt/include \
                -I/usr/openwin/include pgmdemo.c
		$(FF) -o pgmdemo pgmdemo.o -L$(PGPLOT_DIR) -lXmPgplot \
		-L$(PGPLOT_DIR) -lcpgplot -lpgplot -L/usr/dt/lib \
		-lXm -L/usr/openwin/lib -lXt -L/usr/openwin/lib -lX11 \
		$(NSL) $(SOCKET) `/var/spool/pkg/pgplot/cpg/libgcc_path.sh` \
		-lgcc -lm \
		-R$(PGPLOT_DIR):/usr/openwin/lib:/opt/SUNWspro/lib -R/usr/dt/lib

pgdemo4:	$(SRC)/pgdemo4.f
		$(FF) -o pgdemo4 $(SRC)/pgdemo4.f \
		-L$(PGPLOT_DIR) -lcpgplot -lpgplot -L/usr/dt/lib \
		-lXm -L/usr/openwin/lib -lXt -L/usr/openwin/lib -lX11 \
		$(NSL) $(SOCKET) `/var/spool/pkg/pgplot/cpg/libgcc_path.sh` \
		-lgcc -lm \
		-R$(PGPLOT_DIR):/usr/openwin/lib:/opt/SUNWspro/lib -R/usr/dt/lib

d2mat:		$(SRC)/d2mat.F
		$(FF) -o d2mat $(SRC)/d2mat.F \
		-L$(PGPLOT_DIR) -lcpgplot -lpgplot -L/usr/dt/lib \
		-lXm -L/usr/openwin/lib -lXt -L/usr/openwin/lib -lX11 \
		$(NSL) $(SOCKET) `/var/spool/pkg/pgplot/cpg/libgcc_path.sh` \
		-lgcc -lm \
		-R$(PGPLOT_DIR):/usr/openwin/lib:/opt/SUNWspro/lib -R/usr/dt/lib
		d2mat

2dplot:		$(SRC)/2dplot.c $(SRC)/total_entry.mascii.dm \
		$(SRC)/total_entry.mascii \
		2d_fun.o spe_fun.o
		$(cc) $(CCFLAG) -c -I$(PGPLOT_DIR) $(SRC)/2dplot.c
		$(FF) -o $(BIN)/2dplot 2dplot.o 2d_fun.o \
		spe_fun.o \
		-L$(PGPLOT_DIR) $(X11LIB) -lX11 -lcpgplot -lpgplot 
#		2dplot -mat total_entry.mascii -2dg x.gat -cur
#		2dplot -mat total_entry.mascii -fiddle
#		2dplot -mat 152dy_ei.mat
		2dplot -mat total_entry.mascii -fiddle

#---------------------------------------------------------
# utilities
#

fit2spe:	$(SRC)/fit2spe.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/fit2spe $(SRC)/fit2spe.c \
		spe_fun.o -lm
		fit2spe x.dat

addprintf:	$(SRC)/addprintf.c
		$(cc) $(CCFLAG) -o $(BIN)/addprintf  $(SRC)/addprintf.c
		addprintf x.x

mfl:		$(SRC)/mfl.c
		$(cc) $(CCFLAG) -o $(BIN)/mfl $(SRC)/mfl.c
		mfl 
		latex x
		dvips x
		ghostview x.ps

bp:		$(SRC)/bp.c
		$(cc) $(CCFLAG) -o $(BIN)/bp $(SRC)/bp.c
		bp

non_lin_cor:	$(SRC)/non_lin_cor.c
		$(cc) $(CCFLAG) -o $(BIN)/non_lin_cor \
		$(SRC)/non_lin_cor.c -lm
		non_lin_cor /home2/tl/KEEP/gs2k008_nl.dat x.dat

hb:		$(SRC)/hb.c time_stamp.o
		$(GCC)  -o $(BIN)/hb \
		$(SRC)/hb.c time_stamp.o -lm
		hb 

tax:		$(SRC)/tax.c time_stamp.o
		$(cc) $(CCFLAG) -o $(BIN)/tax \
		$(SRC)/tax.c time_stamp.o
		tax

gu_weidenmuller:	$(SRC)/gu_weidenmuller.c
		$(cc) $(CCFLAG) -o $(BIN)/gu_weidenmuller \
		$(SRC)/gu_weidenmuller.c -lm
		gu_weidenmuller 45 36 10 0.6

rho_neu_scale:	$(SRC)/rho_neu_scale.c
		$(cc) $(CCFLAG) -o $(BIN)/rho_neu_scale \
		$(SRC)/rho_neu_scale.c -lm
		rho_neu_scale 35000 0 65 0.47 0.65 3.3 6.5

mean_sigma:	$(SRC)/mean_sigma.c
		$(cc) $(CCFLAG) -o $(BIN)/mean_sigma \
		$(SRC)/mean_sigma.c -lm
		mean_sigma 152dy.y 8 18 1
		xmgr mean_sigma.xy gauss.xy

DECAY_OUT=	$(SRC)/decay_out.F \
		kl_make_array.o kl_e1_gen.o kl_rho.o kl_e2_gen.o \
		kl_cm2_nout.o cleb.o sm3.o get_a_seed.o my_exp.o \
		array_interpolate.o

decay_out:	$(DECAY_OUT)
		$(FF) -o $(BIN)/decay_out \
		$(DECAY_OUT) -lF90

kl_e1_gen:	$(SRC)/kl_e1_gen.F array_interpolate.o kl_prob_get.o \
		kl_rho.o my_exp.o get_a_seed.o sm3.o kl.h
		$(FF) -DPROG  \
		-o $(BIN)/kl_e1_gen $(SRC)/kl_e1_gen.F \
		array_interpolate.o kl_prob_get.o \
		kl_rho.o my_exp.o get_a_seed.o sm3.o

bgfac:		$(SRC)/bgfac.c
		$(cc) $(CCFLAG) -o $(BIN)/bgfac $(SRC)/bgfac.c
		bgfac 1 2 1.0

beep:		$(SRC)/beep.c
		$(cc) $(CCFLAG) -o $(BIN)/beep $(SRC)/beep.c \
		-L/usr/ccs/lib -lcurses
		beep

playfair:	$(SRC)/playfair.c
		$(cc) $(CCFLAG) -o $(BIN)/playfair $(SRC)/playfair.c
		playfair "I live on 802 elm st in washington" "this is a secret message1"

mountjaz:	$(SRC)/mountjaz.c
		$(cc) $(CCFLAG) -o $(BIN)/mountjaz $(SRC)/mountjaz.c

umountjaz:	$(SRC)/umountjaz.c
		$(cc) $(CCFLAG) -o $(BIN)/umountjaz $(SRC)/umountjaz.c

mountUSBdrive:	$(SRC)/mountUSBdrive.c
		$(cc) $(CCFLAG) -o $(BIN)/mountUSBdrive $(SRC)/mountUSBdrive.c

umountUSBdrive:	$(SRC)/umountUSBdrive.c
		$(cc) $(CCFLAG) -o $(BIN)/umountUSBdrive $(SRC)/umountUSBdrive.c

iso_gam:	$(SRC)/iso_gam.c spe_fun.o gasdev.o 2d_fun.o real_theta.o
		$(cc) $(CCFLAG) -o $(BIN)/iso_gam \
		$(SRC)/iso_gam.c spe_fun.o gasdev.o  2d_fun.o real_theta.o -lm
		iso_gam 162 5000 0.048 30 1.3333333 3

eneloss:	eneloss.o plt.o
		$(FF)  -o $(BIN)/eneloss \
		eneloss.o plt.o
eneloss.o:	$(SRC)/eneloss.F
		$(FF)  -c $(SRC)/eneloss.F
plt.o:		$(SRC)/plt.F
		$(FF)  -c $(SRC)/plt.F


#get_a_seed:	$(SRC)/get_a_seed.c
#		$(cc) $(CCFLAG) -o $(BIN)/get_a_seed \
#		$(SRC)/get_a_seed.c
#		get_a_seed

tw_ornl:	$(SRC)/tw_ornl.c
		$(cc) $(CCFLAG) -o $(BIN)/tw_ornl \
		$(SRC)/tw_ornl.c
		tw_ornl

mk_mass_list:	$(SRC)/mk_mass_list.c
		$(cc) $(CCFLAG) -o $(BIN)/mk_mass_list \
		$(SRC)/mk_mass_list.c
		mk_mass_list /home2/tl/KEEP/MASS_RMD.MAS95

cold_search:	$(SRC)/cold_search.c nat_abund.o pda_qval.o
		$(cc) $(CCFLAG) -o $(BIN)/cold_search \
		$(SRC)/cold_search.c nat_abund.o pda_qval.o -lm
		rm -f *.list
		cold_search > x.list
		sort +1n x.list > ria_ria.list
		grep -v Tunstab x.list | sort +1n > ria_stable.list
		grep -v unstab x.list | sort +1n > stable_stable.list
		xmgr ria_ria.list ria_stable.list stable_stable.list -p p

nat_abund:	$(SRC)/nat_abund.c
		$(cc) $(CCFLAG) -o $(BIN)/nat_abund \
		$(SRC)/nat_abund.c
		nat_abund

pda_qval:	$(SRC)/pda_qval.c mass_excess.h  mass_excess_za.h
		$(GCC) $(GCCFLAG) -DPROG -o $(BIN)/pda_qval \
		$(SRC)/pda_qval.c
		pda_qval 6 12 8 16 3 0 0

mass_excess:	$(SRC)/mass_excess.c mass_excess.h  mass_excess_za.h
		$(GCC) $(GCCFLAG) -DPROG -o $(BIN)/mass_excess \
		$(SRC)/mass_excess.c
		mass_excess 6 12 

trans_coeff:	$(SRC)/trans_coeff.c kl_rho.o array_interpolate.o \
		sm3.o my_exp.o
		$(cc) $(CCFLAG) -c $(SRC)/trans_coeff.c
		$(FF) -o $(BIN)/trans_coeff \
		trans_coeff.o kl_rho.o array_interpolate.o \
		sm3.o my_exp.o
		trans_coeff \
		-homega 0.6 -bhight 5 -spin 10 -a 20 -emax 5 -n 100 \
		-level 0 -bshift_i 0.8 -bshift_s 0.4 -moi 70 \
		-vib_rho 4 -vib_emin .3

kl_rho:		$(SRC)/kl_rho.F array_interpolate.o sm3.o my_exp.o
		$(FF) -o $(BIN)/kl_rho -DPROG \
		$(SRC)/kl_rho.F array_interpolate.o sm3.o my_exp.o

kl_rho_new:	$(SRC)/kl_rho_new.F array_interpolate.o sm3.o my_exp.o
		$(FF) -o $(BIN)/$@ -DPROG $^

comp:		$(SRC)/comp.c
		$(cc) $(CCFLAG) -o $(BIN)/comp $(SRC)/comp.c
		comp

foil_e_spread:	$(SRC)/foil_e_spread.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/foil_e_spread \
		$(SRC)/foil_e_spread.c spe_fun.o -lm
		foil_e_spread

interpolate:	$(SRC)/interpolate.c
		$(cc) $(CCFLAG) -o $(BIN)/interpolate $(SRC)/interpolate.c
		interpolate 0 1 1 2 .5

man_gain_cal:	$(SRC)/man_gain_cal.c
		$(cc) $(CCFLAG) -o $(BIN)/man_gain_cal $(SRC)/man_gain_cal.c
		man_gain_cal -178 1.181231 396 800 418.104  843.983  


gdr_ave_fun:	$(SRC)/gdr_ave_fun.F gdr_strength_fun.o \
		gdr_area_strength.o elmsym.o
		$(FF) -o $(BIN)/gdr_ave_fun \
		$(SRC)/gdr_ave_fun.F gdr_strength_fun.o \
		gdr_area_strength.o elmsym.o

gdr_strength_fun.o:	$(SRC)/gdr_strength_fun.F
		$(FF) -c  \
		$(SRC)/gdr_strength_fun.F

gdr_area_strength.o:	$(SRC)/gdr_area_strength.F
		$(FF) -c  \
		$(SRC)/gdr_area_strength.F

gdr_str_fn:	$(SRC)/gdr_str_fn.c
		$(cc) $(CCFLAG) -o $(BIN)/gdr_str_fn \
		$(SRC)/gdr_str_fn.c
		gdr_str_fn 80 112 0.5 15 5 0 30 125

gdrsfun:	$(SRC)/gdrsfun.c
		$(cc) $(CCFLAG) -o $(BIN)/gdrsfun $(SRC)/gdrsfun.c
		gdrsfun 15 15 15 2 2 2 1 1 1 66 88


nn:		$(SRC)/nn.c 
		$(cc) $(CCFLAG) -o $(BIN)/nn \
		$(SRC)/nn.c -lm
#		nn 5 16 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17

add_rings:	$(SRC)/add_rings.c rebel.h spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/add_rings \
		$(SRC)/add_rings.c spe_fun.o
		add_rings det_angles.dat xe_

charge_state:	$(SRC)/charge_state.F
		$(FF) -o $(BIN)/charge_state $(SRC)/charge_state.F 


mk_spe:		$(SRC)/mk_spe.c spe_fun.o rebel_angles.h
		$(cc) $(CCFLAG) -o $(BIN)/mk_spe $(SRC)/mk_spe.c \
		spe_fun.o -lm
		mk_spe peaks.dat 2.09 1.72 0.63 1.33333333 x.spe

be2:		$(SRC)/be2.c
		$(cc) $(CCFLAG) -o $(BIN)/be2 $(SRC)/be2.c -lm

bm1:		$(SRC)/bm1.c
		$(cc) $(CCFLAG) -o $(BIN)/bm1 $(SRC)/bm1.c -lm

polfit.o:	$(SRC)/polfit.f
		$(FF) -c $(SRC)/polfit.f

fit_band:	$(SRC)/fit_band.c polfit.o
		$(cc) -c $(SRC)/fit_band.c
		$(FF) -o $(BIN)/fit_band \
		polfit.o fit_band.o 

pol_fit:	$(SRC)/pol_fit.c polfit.o
		$(cc) -c $(SRC)/pol_fit.c
		$(FF) -o $(BIN)/pol_fit \
		polfit.o pol_fit.o 
		pol_fit x.3 3

lmax:		$(SRC)/lmax.c
		$(cc) -o $(BIN)/lmax $(SRC)/lmax.c -lm

bass_lmax:	$(SRC)/bass_lmax.c
		$(cc) -o $(BIN)/bass_lmax -DPROG \
		$(SRC)/bass_lmax.c -lm
		bass_lmax 34 80 42 76	

camac_adr:	$(SRC)/camac_adr.c
		$(cc) -o $(BIN)/camac_adr $(SRC)/camac_adr.c
#		camac_adr 0xf081f4ea
#		camac_adr 0 1 3 2 16

dopfac:		$(SRC)/dopfac.c
		$(cc) -o $(BIN)/dopfac $(SRC)/dopfac.c -lm

xy2spe:		$(SRC)/xy2spe.c spe_fun.o
		$(cc) -o $(BIN)/xy2spe $(SRC)/xy2spe.c spe_fun.o

y2spe:		$(SRC)/y2spe.c spe_fun.o
		$(cc) -o $(BIN)/y2spe $(SRC)/y2spe.c spe_fun.o

bincount:	$(SRC)/bincount.c
		$(cc) -o $(BIN)/bincount $(SRC)/bincount.c
		bincount

daveps2eps:	$(SRC)/daveps2eps.c
		$(cc) -o $(BIN)/daveps2eps $(SRC)/daveps2eps.c

info:		$(SRC)/info.c $(SRC)/info.h
		$(cc) -o $(BIN)/tst $(SRC)/info.c
		tst

fix_ecal:	$(SRC)/fix_ecal.c $(SRC)/gsII.h \
		time_stamp.o
		$(cc) -o $(BIN)/fix_ecal $(SRC)/fix_ecal.c \
		time_stamp.o

auto_q_find:	$(SRC)/auto_q_find.c spe_fun.o bg_gen_sp.o \
		tlutil.o
		$(cc) -o $(BIN)/auto_q_find $(SRC)/auto_q_find.c \
		spe_fun.o bg_gen_sp.o tlutil.o
#		auto_q_find

iinfo:		$(SRC)/iinfo.c
		$(cc) -o $(TLBIN)/iinfo $(SRC)/iinfo.c
		iinfo 

ave_DO_pt:	$(SRC)/ave_DO_pt.c
		$(cc) -o $(BIN)/ave_DO_pt $(SRC)/ave_DO_pt.c -lm
#		ave_DO_pt DO_data

addcr:		$(SRC)/addcr.c
		$(cc) -o $(BIN)/addcr $(SRC)/addcr.c

hp4550trans:	$(SRC)/hp4550trans.c
		$(cc) -o $(BIN)/hp4550trans $(SRC)/hp4550trans.c

phasertrans:	$(SRC)/phasertrans.c
		$(cc) -o $(BIN)/phasertrans $(SRC)/phasertrans.c

lp_duplex:	$(SRC)/lp_duplex.c
		$(cc) -o $(BIN)/$@ $^

wav2mp3:	$(SRC)/wav2mp3.c
		$(cc) -o $(BIN)/$@ $^
		$(BIN)/$@ aaaa.bbbb.cccc.wav		

make_tcl:	$(SRC)/make_tcl.c
		$(cc) -o $(BIN)/make_tcl $(SRC)/make_tcl.c

hit:		$(SRC)/hit.c
		$(cc) -o $(BIN)/hit $(SRC)/hit.c

gth:		$(SRC)/gth.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/gth $(SRC)/gth.c spe_fun.o

dir_his:	$(SRC)/dir_his.c $(SRC)/his.h
		$(cc) $(CCFLAG) -o $(BIN)/dir_his $(SRC)/dir_his.c

wr_2d_his:	$(SRC)/wr_2d_his.c $(SRC)/his.h
		$(cc) $(CCFLAG) -o $(BIN)/wr_2d_his $(SRC)/wr_2d_his.c -lm 

aiotest:	$(SRC)/aiotest.c 
		$(cc) $(CCFLAG) -o aiotest $(SRC)/aiotest.c
#		aiotest

mknorm:		$(SRC)/mknorm.c 
		$(cc) $(CCFLAG) -o mknorm $(SRC)/mknorm.c
#		mknorm 0.340 -0.1 1000 a1.296 a1.337 a1.377

eff_A0:		$(SRC)/eff_A0.c
		$(cc) $(CCFLAG) -o $(BIN)/eff_A0 \
		$(SRC)/eff_A0.c -lm
#		eff_A0 list 0.334 -0.1

#------------------------------------------------

#gammasphere II tape skimmers

SKIMOBJ = $(SRC)/gs_skim.c time_stamp.o tape_fun.o \
          spe_fun.o get_gsII_ev.o tlutil.o 2d_fun.o \
          gsII_bufp.o skim_fun.o  wr_2d_his.o str_decomp.o \
          info.o gsII_utils.o skim_ext.o create_isomerdata.o\
          get_disk_ev.o get_net_ev.o GSudpGetBuf.o \
          GSudpUtil_r.o BgoCal.o get_a_seed.o rdTimeOffsetFile.o 

SKIMINC = $(SRC)/gsII.h $(SRC)/skim.h $(SRC)/info.h

gs_skim:	$(SKIMOBJ) $(SKIMINC)
		$(cc) $(CCFLAG) -DDEBUG=0 -o $(BIN)/gs_skim \
		-DNORTIME=0 $(SKIMOBJ) -lm $(NSL) $(SOCKET)

gs_tlim:	$(SRC)/gs_tlim.c spe_fun.o tlutil.o \
		sbg.o find_cp_et.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

gs_tlim.c:	skim.h gsII.h	

et_mk_sqare:	et_mk_sqare.c 2d_fun.o 
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^	

et2his:		$(SRC)/et2his.c rd_mat.o wr_2d_his.o spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/et2his \
		$(SRC)/et2his.c rd_mat.o wr_2d_his.o spe_fun.o

et_exam:	$(SRC)/et_exam.c spe_fun.o rd_mat.o tlutil.o \
		skim.h gsII.h find_cp_et.o
		$(cc) $(CCFLAG) -o $(BIN)/et_exam $(SRC)/et_exam.c \
		spe_fun.o rd_mat.o tlutil.o find_cp_et.o

tdir:		$(SRC)/tdir.c tape_fun.o info.o \
		time_stamp.o gsII_utils.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

#------------------------------------------------
# other tape skimmers / primary data analyzers / tape aid utils
#

trew:		$(SRC)/trew.c tape_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/trew $^

gs_ei_skim:	$(SRC)/gs_ei_skim.c time_stamp.o tape_fun.o\
		spe_fun.o skim_fun.o tlutil.o wr_mat.o
		$(cc) $(CCFLAG) -o $(BIN)/gs_ei_skim \
		gs_ei_skim.c -lm time_stamp.o\
		tape_fun.o spe_fun.o skim_fun.o tlutil.o wr_mat.o

dmp_lbl:	$(SRC)/dmp_lbl.c tape_fun.o spe_fun.o\
		time_stamp.o skim_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/dmp_lbl $(SRC)/dmp_lbl.c \
		tape_fun.o spe_fun.o\
		time_stamp.o skim_fun.o

Dmp:		$(SRC)/Dmp.c time_stamp.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

dmp_skim:	$(SRC)/dmp_skim.c get_skim_ev_disk.o \
		get_skim_ev_tape.o skim.h gsII.h info.h \
		get_skim_ev_disk.o get_skim_ev_disk.o time_stamp.o spe_fun.o info.o 
		$(cc) $(CCFLAG) -o $(BIN)/$@ \
		$(SRC)/dmp_skim.c get_skim_ev_tape.o get_skim_ev_disk.o\
		time_stamp.o spe_fun.o info.o

dmp_skim.c:	gsII.h skim.h

get_skim_ev.c:	rebel.h info.h

tape2disk:	$(SRC)/tape2disk.c time_stamp.o tape_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

disk2disk:	$(SRC)/disk2disk.c time_stamp.o tape_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

disk2tape:	$(SRC)/disk2tape.c time_stamp.o tape_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

#-----------------------------------------------
# matrix generation
#

SMATNL = $(SRC)/smatnl.c time_stamp.o wr_mat.o rd_mat.o \
         get_skim_ev.o get_skim_ev_o.o spe_fun.o tape_fun.o \
         tlutil.o info.o

smatnl:		$(SMATNL)
		$(cc) $(CCFLAG) \
		-DFLAG=0 -DDEBUG=0 -o $(BIN)/smatnl $(SMATNL) -lm
		$(cc) $(CCFLAG) \
		-DFLAG=2 -DDEBUG=0 -o $(BIN)/smatnl_o $(SMATNL) -lm

usersub.o:	$(SRC)/usersub.c
		$(cc) -c $(CCFLAG) $(SRC)/usersub.c

mat_make:       $(SRC)/mat_make.c wr_mat.o
		$(cc) $(CCFLAG) \
		-o $(BIN)/mat_make $(SRC)/mat_make.c wr_mat.o

#--------------------------------------------------
# rebel utilities
#

REBEL = $(SRC)/rebel.c time_stamp.o save_rmf.o restore_rmf.o \
        get_skim_ev_disk.o get_skim_ev_tape.o spe_fun.o tape_fun.o get_a_seed.o \
        tlutil.o rebel_malloc.o info.o ellip_lookup.o rebel_external.o \
	str_decomp.o 2d_fun.o rd_tube_q.o

# standard rebel
#
# for now, manually edit rebel.h to have the right MLEN. I
# don't know how to do automatic at the moment
#

rebel_45:	$(REBEL) $(SRC)/rebel.h malloc_mat2dgat.h
		$(cc) $(CCFLAG) \
		-o $(BIN)/rebel_45 $(REBEL)  -lm

rebel_55:	$(REBEL) $(SRC)/rebel.h malloc_mat2dgat.h
		$(cc) $(CCFLAG) \
		-o $(BIN)/rebel_55 $(REBEL)  -lm

rebel_64:	$(REBEL) $(SRC)/rebel.h malloc_mat2dgat.h
		$(cc) $(CCFLAG) \
		-o $(BIN)/rebel_64 $(REBEL)  -lm

rebel_2d:	$(REBEL) $(SRC)/rebel.h REBEL_2D_CODE_2007B.h malloc_mat2dgat.h
		$(cc) $(CCFLAG) -DREBEL_2D \
		-o $(BIN)/rebel_2d $(REBEL)  -lm

save_rmf.o:	$(SRC)/rebel_rmf.c $(SRC)/rebel.h
		$(cc) $(CCFLAG) -DSAVE=1\
		-o save_rmf.o -c $(SRC)/rebel_rmf.c

restore_rmf.o:	$(SRC)/rebel_rmf.c $(SRC)/rebel.h
		$(cc) $(CCFLAG) -DSAVE=0\
		-o restore_rmf.o -c $(SRC)/rebel_rmf.c

rebel_malloc.o:	$(SRC)/rebel_malloc.c $(SRC)/rebel.h malloc_mat2dgat.h
		$(cc) $(CCFLAG) -c $(SRC)/rebel_malloc.c

rebel_external.o:	$(SRC)/rebel_external.c $(SRC)/rebel.h
		$(cc) $(CCFLAG) -c $(SRC)/rebel_external.c

REBEL_ANA.o = time_stamp.o restore_rmf.o spe_fun.o \
              rebel_malloc.o bg_gen_sp.o tlutil.o \
              new_ful.o benlib.o ful_1d_spectrum.o \
              wr_2d_his.o str_decomp.o ellip_lookup.o \
              rebel_bs_FUL.o rd_tube_q.o spe_ag.o 2d_fun.o \
	      save_rmf.o rebel_bs_TL.o 

REBEL_ANA.h =   $(SRC)/rebel.h $(SRC)/benlib.h $(SRC)/his.h \
		$(SRC)/rebel_angles.h

#
# for now, manually edit rebel.h to have the right MLEN. I
# don't know how to do automatic at the moment
#

rebel_ana_45:	$(SRC)/rebel_ana.c $(REBEL_ANA.o) $(REBEL_ANA.h) 
		$(cc) $(CCFLAG) $(SRC)/rebel_ana.c \
		$(REBEL_ANA.o) -o $(BIN)/rebel_ana_45 $(TRAILOR) 

rebel_ana_55:	$(SRC)/rebel_ana.c $(REBEL_ANA.o) $(REBEL_ANA.h) 
		$(cc) $(CCFLAG) $(SRC)/rebel_ana.c \
		$(REBEL_ANA.o) -o $(BIN)/rebel_ana_55 $(TRAILOR) 

rebel_ana_64:	$(SRC)/rebel_ana.c $(REBEL_ANA.o) $(REBEL_ANA.h) 
		$(cc) $(CCFLAG) $(SRC)/rebel_ana.c \
		$(REBEL_ANA.o) -o $(BIN)/rebel_ana_64 $(TRAILOR) 



rebel_add:	$(SRC)/rebel_add.c $(SRC)/rebel.h \
		time_stamp.o  spe_fun.o 
		$(cc) $(CCFLAG) $(SRC)/rebel_add.c \
		-o $(BIN)/rebel_add time_stamp.o spe_fun.o 
#		rebel_add test.rmf.t1 test.rmf.t1  test.rmf

rebel_veto:	$(SRC)/rebel_veto.c $(SRC)/rebel.h \
		time_stamp.o  spe_fun.o  bg_gen_sp.o tlutil.o
		$(cc) $(CCFLAG) $(SRC)/rebel_veto.c \
		-o $(BIN)/rebel_veto time_stamp.o spe_fun.o \
		bg_gen_sp.o tlutil.o
#		rebel_veto 1.05

dopcoradd:	$(SRC)/dopcoradd.c spe_fun.o spe_ag.o tlutil.o rebel_angles.h
		$(cc) $(CCFLAG) -o $(BIN)/dopcoradd $(SRC)/dopcoradd.c \
		spe_fun.o  spe_ag.o tlutil.o -lm
		dopcoradd 0.04728 -.23 g_0203- .spe x.spe

#------------------------------------------------
# general matrix utilities
#

check_host:	$(SRC)/check_host.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		check_host sun0

check_house:	$(SRC)/check_house.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^

gg:	        $(SRC)/gg.c spe_fun.o rd_mat.o sum_counts.o\
		mat_sum_stat.o dcmd.o
		$(cc) $(CCFLAG) -o $(BIN)/gg \
		-DDCE_TYPE=0 $(SRC)/gg.c \
		spe_fun.o rd_mat.o\
		sum_counts.o mat_sum_stat.o dcmd.o -lm

gg_new:	        $(SRC)/gg.c spe_fun.o rd_mat.o sum_counts.o\
		mat_sum_stat.o dcmd.o
		$(cc) $(CCFLAG) -o $(BIN)/gg_new \
		-DDCE_TYPE=1 $(SRC)/gg.c \
		spe_fun.o rd_mat.o\
		sum_counts.o mat_sum_stat.o dcmd.o -lm

tp:		$(SRC)/tp.c rd_mat.o mat_sum_stat.o spe_fun.o wr_2d_his.o
		$(cc) $(CCFLAG) -o $(BIN)/tp $(SRC)/tp.c rd_mat.o \
		mat_sum_stat.o spe_fun.o wr_2d_his.o

mad:		$(SRC)/mad.c rd_mat.o mat_sum_stat.o wr_mat.o
		$(cc) $(CCFLAG) -o $(BIN)/mad $(SRC)/mad.c \
		rd_mat.o mat_sum_stat.o wr_mat.o

tp_bg:		$(SRC)/tp_bg.c rd_mat.o mat_sum_stat.o spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/tp_bg $(SRC)/tp_bg.c\
		rd_mat.o mat_sum_stat.o spe_fun.o

pb_rat:         $(SRC)/pb_rat.c spe_fun.o sum_counts.o
		$(cc) $(CCFLAG) -o $(BIN)/pb_rat $(SRC)/pb_rat.c \
		spe_fun.o sum_counts.o

gatlist:	$(SRC)/gatlist.c
		$(cc) $(CCFLAG) -o $(BIN)/gatlist $(SRC)/gatlist.c

#-----------------------------------------------
# cube generation
#
qq:		$(SRC)/qq.c $(SRC)/slice.h
		$(cc) -DDATE=\"`date`\" $(CCFLAG) -o $(BIN)/qq $(SRC)/qq.c

qu:		$(SRC)/qu.c
		$(cc) -DDATE=\"`date`\" $(CCFLAG) -o $(BIN)/qu $(SRC)/qu.c

sq4:		$(SRC)/sq4.c
		$(cc) -DDATE=\"`date`\" $(CCFLAG) -o $(BIN)/sq4 $(SRC)/sq4.c

#------------------------------------------------
# special matrix utilities
#

cr_di:		$(SRC)/cr_di.c wr_ascii.o rd_mat.o spe_fun.o sum_counts.o
		$(cc)   $(CCFLAG) -o $(BIN)/cr_di \
		$(SRC)/cr_di.c wr_ascii.o rd_mat.o spe_fun.o\
		sum_counts.o -lm

enc:		$(SRC)/enc.c
		$(cc) $(CCFLAG) -DDOS=0 -o $(BIN)/enc $(SRC)/enc.c

#------------------------------------------------
# spectrum utilities
#

FindTOffset:	FindTOffset.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		FindTOffset -prefix HKtbgo -dch 70 -outfile tbgo.cal -alignat 2000

target_offset:	$(SRC)/target_offset.c real_theta.o rebel_angles.h
		$(cc) $(CCFLAG) -o $(BIN)/target_offset real_theta.o\
		$(SRC)/target_offset.c -lm
		target_offset 1255.05 0.028300 -0.900 data.xydy

relnorm_util:	$(SRC)/relnorm_util.c
		$(cc) $(CCFLAG) -o $(BIN)/relnorm_util \
		$(SRC)/relnorm_util.c -lm

agpar:		$(SRC)/agpar.c
		$(cc) $(CCFLAG) -o $(BIN)/agpar agpar.c
#		agpar 0.33333 1.33333 0 1

mkpeaks:	$(SRC)/mkpeaks.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/mkpeaks \
		$(SRC)/mkpeaks.c spe_fun.o -lm
#		mkpeaks 5632 test.spe 10 100000 10 5000 1000000 10


mno:		$(SRC)/mno.c
		$(cc) $(CCFLAG) -o $(BIN)/mno $(SRC)/mno.c -lm

ano:		$(SRC)/ano.c
		$(cc) $(CCFLAG) -o $(BIN)/ano $(SRC)/ano.c -lm

dno:		$(SRC)/dno.c
		$(cc) $(CCFLAG) -o $(BIN)/dno $(SRC)/dno.c -lm

R-factor:	$(SRC)/R-factor.c
		$(cc) $(CCFLAG) -o $(BIN)/R-factor $(SRC)/R-factor.c -lm
		R-factor 1.1 2.08 33.5

ar1:		$(SRC)/ar1.c spe_ag.o spe_fun.o tlutil.o spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/ar1 $(SRC)/ar1.c spe_ag.o \
		spe_fun.o tlutil.o -lm
#		ar1 logp x x1
#		ar1 cts  x 5.0 y 1
#		cp x.spe y.spe; ar1 dcr y 566 584 587 592 597 614
#		ar1 spl x 900 20 1 y z 
#		ar1 chi2 data fit 4700 5300 -6667.105 1.333421
#		xmgrace -type xydy ar1chi2_data.xydy -type xy ar1chi2_fit.xy
		ar1 ct x 2 y

ar2:		$(SRC)/ar2.c 2d_fun.o mat_analyze.o spe_fun.o \
		bg_gen2_2d.o bg_gen_sp2.o tlutil.o kernel_enhance.o \
		do_pc_2d.o do_stripe_2d.o zappeaks_2d.o zapgrid_2d.o\
		rmpeaks_2d.o get_a_seed.o diagpro.o meandiagpro.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ -lm
#		ar2 evapORIE 152dy_ei.mat 5 x.mat
#		ar2 rmpeaks SD.mat SD_rmpeaks.peaks x.mat 2
#	ar2 rmslin 10.mat x.suml x.mat

zappeaks_2d.o:	$(SRC)/zappeaks_2d.c
		$(cc) $(CCFLAG) -c $^

zapgrid_2d.o:	$(SRC)/zapgrid_2d.c
		$(cc) $(CCFLAG) -c $^

mk_ridge:	$(SRC)/mk_ridge.c spe_fun.o 2d_fun.o gasdev.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ -lm

mk_ridge1:	$(SRC)/mk_ridge1.c spe_fun.o 2d_fun.o gasdev.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ -lm

mk_ridge2:	$(SRC)/mk_ridge2.c spe_fun.o 2d_fun.o gasdev.o get_a_seed.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ -lm
#		mk_ridge2 83 50 60 5 7 230 0.2 10000 1 x.mat 2048 0.1 2 1.333333 0

mk_bands_2d:	$(SRC)/mk_bands_2d.c spe_fun.o 2d_fun.o gasdev.o
		$(cc) $(CCFLAG) -o $(BIN)/mk_bands_2d $(SRC)/mk_bands_2d.c \
		spe_fun.o 2d_fun.o gasdev.o -lm
#		mk_bands_2d 152dy_sd.band 1000 2048 1.33333 0.5 x.mat

mk_QC_2d:	$(SRC)/mk_QC_2d.c spe_fun.o 2d_fun.o gasdev.o
		$(cc) $(CCFLAG) -o $(BIN)/mk_QC_2d $(SRC)/mk_QC_2d.c \
		spe_fun.o 2d_fun.o gasdev.o -lm
		mk_QC_2d st.spe 2048 1 x.mat

moi:		$(SRC)/moi.c
		$(cc) $(CCFLAG) -o $(BIN)/moi $(SRC)/moi.c -lm
#		moi 151

moi2:		$(SRC)/moi2.c
		$(cc) $(CCFLAG) -o $(BIN)/moi2 $(SRC)/moi2.c -lm
#		moi2 192 0.46 0

moi3:		$(SRC)/moi3.c
		$(cc) $(CCFLAG) -o $(BIN)/moi3 $(SRC)/moi3.c -lm

gta:		$(SRC)/gta.c spe_fun.o wr_ascii.o 
		$(cc) $(CCFLAG) -o $(BIN)/gta $(SRC)/gta.c spe_fun.o wr_ascii.o

stxydy:		$(SRC)/stxydy.c spe_fun.o wr_ascii.o
		$(cc) $(CCFLAG) -o $(BIN)/stxydy $(SRC)/stxydy.c spe_fun.o \
		wr_ascii.o -lm

stxy:		$(SRC)/stxy.c spe_fun.o wr_ascii.o
		$(cc) $(CCFLAG) -o $(BIN)/stxy $(SRC)/stxy.c \
		spe_fun.o wr_ascii.o

ascii_area_xy:	$(SRC)/ascii_area_xy.c	
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
#		$(BIN)/$@ alleg.xy 0.1 30

rel_err:	$(SRC)/rel_err.c spe_fun.o wr_ascii.o
		$(cc) $(CCFLAG) -o $(BIN)/rel_err $(SRC)/rel_err.c \
		spe_fun.o wr_ascii.o

sum_xy_spectra:	$(SRC)/sum_xy_spectra.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$@ 0.5 lo.xy 0.5 hi.xy		

atg:		$(SRC)/atg.c spe_fun.o rd_ascii.o
		$(cc) $(CCFLAG) -o $(BIN)/atg $(SRC)/atg.c spe_fun.o rd_ascii.o

bg_gen:		$(SRC)/bg_gen.c spe_fun.o tlutil.o bg_gen_sp.o
		$(cc) $(CCFLAG) -o $(BIN)/bg_gen $(SRC)/bg_gen.c spe_fun.o \
		tlutil.o bg_gen_sp.o 

bg_gen2:	$(SRC)/bg_gen2.c spe_fun.o tlutil.o bg_gen_sp2.o
		$(cc) $(CCFLAG) -o $(BIN)/bg_gen2 $(SRC)/bg_gen2.c spe_fun.o \
		tlutil.o bg_gen_sp2.o -lm
		bg_gen2 test  20 2.0  1.1 x

bg_gen2_2d.o:	$(SRC)/bg_gen2_2d.c 
		$(cc) $(CCFLAG) -c  $(SRC)/bg_gen2_2d.c 

auto_ad:	$(SRC)/auto_ad.c spe_fun.o 
		$(cc) $(CCFLAG) -o $(BIN)/auto_ad $(SRC)/auto_ad.c \
		spe_fun.o -lm 		

test_hs1:	$(SRC)/test_hs1.c rd_hs1.o spe_fun.o hs1.h
		$(cc) $(CCFLAG) -o test_hs1  test_hs1.c  spe_fun.o rd_hs1.o
#		test_hs1

mktsttape:	$(SRC)/mktsttape.c ev_gen.o gasdev.o spe_fun.o
		$(cc) $(CCFLAG) -o mktsttape \
		$(SRC)/mktsttape.c ev_gen.o gasdev.o spe_fun.o -lm
#		trew /dev/nrst9 0
#		mktsttape
#		trew /dev/nrst9 0
#		rm -f x.x
#		dmp_skim_o /dev/nrst9 100 100 -35 35 > x.x
#		trew /dev/nrst9 0

#------------------------------------------------
#

LoadedDipole:	$(SRC)/LoadedDipole.c HamLib.o
		$(cc) $(CCFLAG) -o $(BIN)/LoadedDipole \
		LoadedDipole.c HamLib.o -lm
		LoadedDipole 3.95 0.05 0.98 206.7 3.6 2.05 138.0 0.15 0.03 200 10 2

CEAmp:		$(SRC)/CEAmp.c HamLib.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		$(BIN)/$@

findLC:		$(SRC)/findLC.c HamLib.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		$(BIN)/$@

LC:		$(SRC)/LC.c HamLib.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		LC -C 300 -L 200

eFolAmp:	$(SRC)/eFolAmp.c HamLib.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		eFolAmp

stdRval:	$(SRC)/stdRval.c HamLib.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		$(BIN)/$@ 1.49

adjRidge:	$(SRC)/adjRidge.c 
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		$(BIN)/$@ cur_exp_ridge.xy 750_x-y.xy -300 -200 -46 -22 x.xy
		xmgrace cur_exp_ridge.xy 750_x-y.xy x.xy

HamLib.o:	$(SRC)/HamLib.c
		$(cc) $(CCFLAG) -c $(SRC)/HamLib.c

hfc:		hfg.c GetSwDef.o
		gcc -DFIND -o $(BIN)/$@ -I /usr/local/mysql/include/mysql \
		-L /usr/local/mysql/lib/mysql $^ -lmysqlclient
		hfc ve9gu

hfa:		hfg.c GetSwDef.o
		gcc -DADD -o $(BIN)/$@ -I /usr/local/mysql/include/mysql \
		-L /usr/local/mysql/lib/mysql $^ -lmysqlclient
		hfa 2002 1

cwprep:		cwprep.c
		gcc -o $(BIN)/$@ $^
		$(BIN)/$@ < test.txt		

#------------------------------------------------
# various other utilities
#

revline:	$(SRC)/revline.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@ < x.dat

rd_ces:		$(SRC)/rd_ces.c 
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		rd_ces BADCES_0_001_000006.ces

CESevRead.o:	CESevRead.c sga.h dataFormats.h
		$(cc) $(CCFLAG) -c $^	

cesjumpers:	$(SRC)/cesjumpers.c 
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@

sga_sun:	$(SRC)/sga_sun.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@

ascii_gain_off:	$(SRC)/ascii_gain_off.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@ x.xy 0 1 0 2 0 1 y.xy

ascii_smooth:	$(SRC)/ascii_smooth.c tlutil.o
		$(cc) $(ccFLAG) -o $(BIN)/$@ $^
		$(BIN)/$@ x.xy 3 y.xy


kp:		$(SRC)/kp.c get_a_seed.o
		$(cc) $(CCFLAG) -o $(BIN)/kp $(SRC)/kp.c \
		get_a_seed.o -lcurses
		kp /home/tl/pr/ka/kicks j 10 x

diskuse:	$(SRC)/diskuse.c
		$(cc)   $(CCFLAG) -o $(BIN)/diskuse $(SRC)/diskuse.c
#		diskuse /tmp/use.log

find_vc:	find_vc.c
		$(cc)   $(CCFLAG) -o $(BIN)/find_vc find_vc.c -lm

calc_vc:	calc_vc.c
		$(cc) $(CCFLAG) -o $(BIN)/calc_vc calc_vc.c -lm
		calc_vc 57.3 152

calc_er:	calc_er.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $^ -lm
		calc_er 191 48 156 152

j2:		$(SRC)/j2.c
		$(cc)   $(CCFLAG) -o $(BIN)/j2 $(SRC)/j2.c -lm
		j2 x.dat j2.xydy j1.xy

best_vc:	best_vc.c spe_fun.o
		$(cc) $(CCFLAG) -o /home/tl/bin/best_vc \
#		best_vc.c spe_fun.o

prob_to_intens:	$(SRC)/prob_to_intens.c
		$(cc) $(CCFLAG) -o $(BIN)/prob_to_intens \
		$(SRC)/prob_to_intens.c
#		prob_to_intens p.dat x.xy

qs:		$(SRC)/qs.c time_stamp.o
		$(cc) $(CCFLAG) -o $(BIN)/qs $(SRC)/qs.c time_stamp.o

pchop:		$(SRC)/pchop.c spe_fun.o
		$(cc) -o $(BIN)/pchop $(SRC)/pchop.c spe_fun.o -lm

a0a2a4:		$(SRC)/a0a2a4.f gaga_io.o legfit.o upcase.o
		$(FF) -o $(BIN)/a0a2a4 \
		$(SRC)/a0a2a4.f gaga_io.o legfit.o upcase.o

angdis:		$(SRC)/angdis.f legfit.o
		$(FF) -o $(BIN)/angdis $(SRC)/angdis.f \
		legfit.o -lm

e1e2:		$(SRC)/e1e2.c spe_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/e1e2 \
		$(SRC)/e1e2.c spe_fun.o

extra_xy:       $(SRC)/extra_xy.c
		$(cc) $(CCFLAG) -o $(BIN)/extra_xy $(SRC)/extra_xy.c
#		extra_xy y.dat 1 3 1 3 z.dat
#		cat z.dat

mod_data:       $(SRC)/mod_data.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^ -lm
		mod_data x.x 

rd_pace:       $(SRC)/rd_pace.c
		$(cc) $(CCFLAG) -o $(BIN)/$@ $(SRC)/$^ -lm
		rd_pace x.dat 

band:		$(SRC)/band.c
		$(cc) $(CCFLAG) -o $(BIN)/band $(SRC)/band.c -lm
		band x.1 24 2 10646 

#rot_damp_width:	$(SRC)/rot_damp_width.c npa547_rotd.o
#		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
#		rot_damp_width 152 60 0.56 0 5 25 0.5

mk_seniority:	$(SRC)/mk_seniority.c 2d_fun.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		mk_seniority 152 x.mat

mk_deltaomega:	$(SRC)/mk_deltaomega.c  2d_fun.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm

mk_rotd_wid:	$(SRC)/mk_rotd_wid.c npa547_rotd.o 2d_fun.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
#		mk_rotd_wid 152 0.56 0.5 x.mat 1 1.0
		mk_rotd_wid 194 0.46 0.5 x.mat 1 2.0

mk_rotd_nar:	$(SRC)/mk_rotd_nar.c 2d_fun.o
		$(cc) $(CCFLAG) $^ -o $(BIN)/$@ -lm
		mk_rotd_nar 152 x.mat

mk_rotd_frac:	mk_rotd_frac.o kl_rho.o  my_exp.o spe_fun.o tlutil.o \
		array_interpolate.o 2d_fun.o sm3.o 
		$(FF) $^ -o $(BIN)/$@

ang_cor:	ang_cor.c str_decomp.o	get_a_seed.o
		$(cc) $(CCFLAG) -lm  $^ -o $(BIN)/$@
#		$(BIN)/$@ 0.334 -0.1 0.00174 "10-51,53-57,59-110"
#		$(BIN)/$@  0.2957 0 0.00125 "1-110"
#		$(BIN)/$@ -0.1135 0 0.00125 "1-110"
		$(BIN)/$@  0.2952 0 0.00134 0.000604 "7-9,11-32,34-52,54-57,59-110"
		$(BIN)/$@ -0.1133 0 0.00134 0.000645 "7-9,11-32,34-52,54-57,59-110"
#		$(BIN)/$@ 0.0 0.0 0.0 "10-51,53-57,59-110"

ang_cor.c:	ang_cor.c gs_detector_angles.h	idtable.h					

mk_rotd_const:	mk_rotd_const.c	2d_fun.o
		$(cc) $(CCFLAG)  $^ -o $(BIN)/$@		

band_diff:	$(SRC)/band_diff.c
		$(cc) $(CCFLAG) -o $(BIN)/band_diff $(SRC)/band_diff.c -lm
		band_diff sd6.xy sd1.xy 

band_feed:	$(SRC)/band_feed.c
		$(cc) $(CCFLAG) -o $(BIN)/band_feed $(SRC)/band_feed.c -lm
		band_feed x.x

possible_gam:	$(SRC)/possible_gam.c
		$(cc) $(CCFLAG) -o $(BIN)/possible_gam $(SRC)/possible_gam.c -lm
		possible_gam x.1 x.2

band_vs_ref:	$(SRC)/band_vs_ref.c
		$(cc) $(CCFLAG) -o $(BIN)/band_vs_ref $(SRC)/band_vs_ref.c -lm
		band_vs_ref x.1 0.007 

wmean:		$(SRC)/wmean.c
		$(cc) $(CCFLAG) -o $(BIN)/wmean $(SRC)/wmean.c -lm
		wmean x.dat 

amean:		$(SRC)/amean.c
		$(cc) $(CCFLAG) -o $(BIN)/amean $(SRC)/amean.c -lm
		amean x.dat 

dedi_calc:	$(SRC)/dedi_calc.c
		$(cc) -o $(BIN)/$@ $^
		dedi_calc x.dat		

ascii_offset:	$(SRC)/ascii_offset.c
		$(cc) $(CCFLAG) -o $(BIN)/ascii_offset $(SRC)/ascii_offset.c -lm
		ascii_offset x.dat 2

mod_bin:	$(SRC)/mod_bin.c
		$(cc) $(CCFLAG) -o $(BIN)/mod_bin $(SRC)/mod_bin.c -lm
		mod_bin < TADLog.pdb.orig


prep_fgh_fit:	$(SRC)/prep_fgh_fit.c
		$(cc) $(CCFLAG) -o $(BIN)/prep_fgh_fit $(SRC)/prep_fgh_fit.c -lm


lbl_ship_db:	$(SRC)/lbl_ship_db.c
		$(cc) $(CCFLAG) -o $(BIN)/lbl_ship_db $(SRC)/lbl_ship_db.c -lm
		lbl_ship_db totinv.dta

w_angfac:	$(SRC)/w_angfac.c
		$(cc) $(CCFLAG) -o $(BIN)/w_angfac -lm $(SRC)/w_angfac.c -lm

txt2doc:	$(SRC)/txt2doc.c
		$(cc) $(CCFLAG) -o $(BIN)/txt2doc -DMAC=0 $(SRC)/txt2doc.c

colorps2bwps:	$(SRC)/colorps2bwps.c
		$(cc) $(CCFLAG) -o $(BIN)/colorps2bwps $(SRC)/colorps2bwps.c 
#		colorps2bwps proc.ps

#------------------------------------------------
atmat:		$(SRC)/atmat.c wr_mat.o
		$(cc) $(CCFLAG) -o $(BIN)/atmat $(SRC)atmat.c wr_mat.o

mnc:		$(SRC)/mnc.c spe_fun.o rd_mat.o mat_sum_stat.o tlutil.o
		$(cc) $(CCFLAG) -o $(BIN)/mnc mnc.c spe_fun.o\
		rd_mat.o mat_sum_stat.o tlutil.o

tc:		$(SRC)/tc.c efftape.h time_stamp.o tape_fun.o
		$(cc) $(CCFLAG) -o $(BIN)/tc $(SRC)/tc.c time_stamp.o tape_fun.o
#		tc /dev/rmt/0mbn /dev/rmt/1mbn 1 10 0 112 90 16384

cal_test:	$(SRC)/tcal_test.c rd_ascii.o ppos.o
		$(cc) $(CCFLAG) -o $(BIN)/cal_test $(SRC)/cal_test.c \
		ppos.o rd_ascii.o

fr_skim:	$(SRC)/fr_skim.f get_skim_ev.o
		$(FF) $(CCFLAG) -o $(BIN)/fr_skim get_skim_ev.o $(SRC)/fr_skim.f

mat_tp:         $(SRC)/mat_tp.c rd_mat.o wr_ascii.o 
		$(cc) $(CCFLAG) -o $(BIN)/mat_tp $(SRC)/mat_tp.c \
		rd_mat.o wr_ascii.o

mat_sub:        $(SRC)/mat_sub.c rd_mat.o wr_mat.o mat_sum_stat.o
		$(cc) $(CCFLAG) -o $(BIN)/ mat_sub $(SRC)/mat_sub.c rd_mat.o \
		wr_mat.o mat_sum_stat.o

i4toi2:		$(SRC)/i4toi2.c rd_mat.o mat_sum_stat.o
		$(cc) $(CCFLAG) -o $(BIN)/i4toi2 $(SRC)/i4toi2.c \
		rd_mat.o mat_sum_stat.o

i_missed:	$(SRC)/i_missed.c rd_tube_q.o
		$(cc) $(CCFLAG) -o $(BIN)/i_missed $(SRC)/i_missed.c \
		rd_tube_q.o
		i_missed 19 q4.q intensity-file 4 7

mkBGOefitcmd:	$(SRC)/mkBGOefitcmd.c
		$(cc) -o $(BIN)/$@ $(CCFLAG) $^
		$(BIN)/$@ 20 22

rdBGOgf3sto:	$(SRC)/rdBGOgf3sto.c
		$(cc) -o $(BIN)/$@ $(CCFLAG) $^
		$(BIN)/$@ raw_cal.data bgo_veto.list 4.0 bgo.cal

#
#---------------------------------------------------------------------
# Ben's FULL method (taken from /dk/gam1/disk2/bgo/util/src/crowellware/
#                    on 11/5/95)

new_ful.o:	$(SRC)/new_ful.c $(SRC)/what_machine.h \
		$(SRC)/benlib.h $(SRC)/standardize.h
		$(cc)  -c $(CCFLAG) $(SRC)/new_ful.c

benlib.o:	$(SRC)/benlib.c $(SRC)/benlib.h
		$(cc)  -c $(CCFLAG) $(SRC)/benlib.c

ful_1d_spectrum.o:	$(SRC)/ful_1d_spectrum.c 
		$(cc)  -c $(CCFLAG) $(SRC)/ful_1d_spectrum.c

#---------------------------------------------------------------------
# basic functions
#


mk_rotd_frac.o:	$(SRC)/mk_rotd_frac.c
		$(cc) -c $(CCFLAG) $^

read_rotd_mat.o:	$(SRC)/read_rotd_mat.c
		$(cc) -c $(CCFLAG) $^

npa547_rotd.o:	$(SRC)/npa547_rotd.c
		$(cc) -c $(CCFLAG) $^

rmpeaks_2d.o:	$(SRC)/rmpeaks_2d.c
		$(cc) -c $(CCFLAG) $(SRC)/rmpeaks_2d.c

get_a_seed.o:	$(SRC)/get_a_seed.c 
		$(cc) -c $(CCFLAG) $(SRC)/get_a_seed.c

get_a_seedCC.o:	$(SRC)/get_a_seed.c 
		$(CC) -c $(CCFLAG) -o get_a_seedCC.o $(SRC)/get_a_seed.c

skim_ext.o:	$(SRC)/skim_ext.c skim.h
		$(cc) -c $(CCFLAG) $(SRC)/skim_ext.c

mat_analyze.o:	$(SRC)/mat_analyze.c
		$(cc) -c $(CCFLAG) $(SRC)/mat_analyze.c

wr_mat.o:	$(SRC)/wr_mat.c
		$(cc) -c $(CCFLAG) $(SRC)/wr_mat.c

wr_mat_sh.o:	$(SRC)/wr_mat_sh.c
		$(cc)  -c $(CCFLAG) $(SRC)/wr_mat_sh.c

rd_mat.o:       $(SRC)/rd_mat.c
		$(cc) -c $(CCFLAG) $(SRC)/rd_mat.c

wr_ascii.o:     $(SRC)/wr_ascii.c
		$(cc) -c $(CCFLAG) $(SRC)/wr_ascii.c

rd_ascii.o:     $(SRC)/rd_ascii.c
		$(cc) -c $(CCFLAG) $(SRC)/rd_ascii.c

diagpro.o:	$(SRC)/diagpro.c
		$(cc) -c $(CCFLAG) $^		

meandiagpro.o:	$(SRC)/meandiagpro.c
		$(cc) -c $(CCFLAG) $^		

sum_counts.o:	$(SRC)/sum_counts.c
		$(cc) -c $(CCFLAG) $(SRC)/sum_counts.c

mat_sum_stat.o:	$(SRC)/mat_sum_stat.c
		$(cc) -c $(CCFLAG) $(SRC)/mat_sum_stat.c

get_skim_ev_tape.o:	$(SRC)/get_skim_ev.c info.o 
		$(cc) -c $(CCFLAG) -DTAPE $(SRC)/get_skim_ev.c \
		-o $@

get_skim_ev_disk.o:	$(SRC)/get_skim_ev.c info.o 
		$(cc) -c $(CCFLAG) -DDISK $(SRC)/get_skim_ev.c \
		-o $@ 

lbl_ev.o:	$(SRC)/lbl_ev.c
		$(cc) -c $(CCFLAG) $(SRC)/lbl_ev.c

ppos.o:		$(SRC)/ppos.c
		$(cc) -c $(CCFLAG) $(SRC)/ppos.c

tlutil.o:	$(SRC)/tlutil.c
		$(cc)  -c $(CCFLAG) $(SRC)/tlutil.c

dcmd.o:		$(SRC)/dcmd.c
		$(cc) -c $(CCFLAG) $(SRC)/dcmd.c

skim_fun.o:	$(SRC)/skim_fun.c
		$(cc) -c $(CCFLAG) $(SRC)/skim_fun.c

sbg.o:		$(SRC)/sbg.c tlutil.o
		$(cc) -c $(CCFLAG) $(SRC)/sbg.c tlutil.o	

wr_his1:	$(SRC)/wr_his1.c
		$(cc) -g -o wr_his1 $(CCFLAG) $(SRC)/wr_his1.c
#		wr_his1

spe_ag.o:	$(SRC)/spe_ag.c
		$(cc) -c $(CCFLAG) $(SRC)/spe_ag.c

put_mat.o:	$(SRC)/put_mat.c
		$(cc) -c $(CCFLAG) $(SRC)/put_mat.c

process_ang.o:	$(SRC)/process_ang.c
		$(cc) -c $(CCFLAG) $(SRC)/process_ang.c

gaga_io.o:	$(SRC)/gaga_io.f
		$(FF) -c $(SRC)/gaga_io.f 

legfit.o:	$(SRC)/legfit.f
		$(FF) -c $(SRC)/legfit.f

upcase.o:	$(SRC)/upcase.f
		$(FF) -c $(SRC)/upcase.f

bg_gen_sp.o:	$(SRC)/bg_gen_sp.c
		$(cc) -c $(CCFLAG) $(SRC)/bg_gen_sp.c

wr_2d_his.o:	$(SRC)/wr_2d_his.c $(SRC)/his.h
		$(cc) -c $(CCFLAG) $(SRC)/wr_2d_his.c

find_cp_et.o:	$(SRC)/find_cp_et.c skim.h gsII.h
		$(cc) -c $(CCFLAG) $(SRC)/find_cp_et.c

str_decomp.o:	$(SRC)/str_decomp.c
		$(cc) -c $(CCFLAG) $(SRC)/str_decomp.c

str_decompCC.o:	$(SRC)/str_decomp.c
		$(CC) -c $(CCFLAG) $(SRC)/str_decomp.c \
		-o str_decompCC.o

rd_hs1.o:	$(SRC)/rd_hs1.c hs1.h
		$(cc) -c $(CCFLAG) $(SRC)/rd_hs1.c

rd_damm_ban.o:	$(SRC)/rd_damm_ban.c
		$(cc) $(CCFLAG) -c $(SRC)/rd_damm_ban.c

bgo_ev.o:	$(SRC)/bgo_ev.c bgo.h
		$(cc) $(CCFLAG) -c $(SRC)/bgo_ev.c

info.o:		$(SRC)/info.c $(SRC)/info.h
		$(cc) $(CCFLAG) -c $(SRC)/info.c

ev_gen.o:	$(SRC)/ev_gen.c
		$(cc) $(CCFLAG) -c $(SRC)/ev_gen.c

gasdev.o:	$(SRC)/gasdev.c 
		$(cc) $(CCFLAG) -c $(SRC)/gasdev.c

ellip_lookup.o:	$(SRC)/ellip_lookup.c rebel.h
		$(cc) $(CCFLAG) -c $(SRC)/ellip_lookup.c

rebel_bs_FUL.o:	$(SRC)/rebel_bs_FUL.c $(SRC)/rebel.h $(SRC)/his.h 
		$(cc) $(CCFLAG) -c $(SRC)/rebel_bs_FUL.c

rebel_bs_TL.o:	$(SRC)/rebel_bs_TL.c $(SRC)/rebel.h $(SRC)/his.h 
		$(cc) $(CCFLAG) -c $(SRC)/rebel_bs_TL.c

rd_tube_q.o:	$(SRC)/rd_tube_q.c $(SRC)/rebel.h 
		$(cc) $(CCFLAG) -c $(SRC)/rd_tube_q.c

2d_fun.o:	$(SRC)/2d_fun.c
		$(cc) $(CCFLAG) -c $(SRC)/2d_fun.c

bg_gen_sp2.o:	$(SRC)/bg_gen_sp2.c
		$(cc) $(CCFLAG) -c $(SRC)/bg_gen_sp2.c -lm

kernel_enhance.o:	$(SRC)/kernel_enhance.c
		$(cc) $(CCFLAG) -c $(SRC)/kernel_enhance.c -lm

do_pc_2d.o:	$(SRC)/do_pc_stripe.c
		$(cc) $(CCFLAG) -DPC -c -o do_pc_2d.o $(SRC)/do_pc_stripe.c 

do_stripe_2d.o:	$(SRC)/do_pc_stripe.c
		$(cc) $(CCFLAG) -DSTRIPE -c -o do_stripe_2d.o $(SRC)/do_pc_stripe.c 

kl_rho.o:	$(SRC)/kl_rho.F
		$(FF) -c $(SRC)/kl_rho.F

goe_level_sel.o:	$(SRC)/goe_level_sel.F
		$(FF) -c $(SRC)/goe_level_sel.F

nat_abund.o:	$(SRC)/nat_abund.c
		$(cc) $(CCFLAG) -c $(SRC)/nat_abund.c

pda_qval.o:	$(SRC)/pda_qval.c mass_excess.h  mass_excess_za.h
		$(cc) $(CCFLAG) -c $(SRC)/pda_qval.c

real_theta.o:	$(SRC)/real_theta.c
		$(cc) $(CCFLAG) -c $(SRC)/real_theta.c

rdTimeOffsetFile.o:	$(SRC)/rdTimeOffsetFile.c
		$(cc) $(CCFLAG) -c $^		

#-----------------------------------------------
# fortran functions

top_iwrite.o:	$(SRC)/top_write.F
		$(FF) -DIVER -c $< -o $@

top_rwrite.o:	$(SRC)/top_write.F
		$(FF) -DRVER -c $< -o $@

#-----------------------------------------------
# default

%.o:		$(SRC)/%.F
		$(FF) -c $<


#-------------------------------------------
# cleaning etc.

clean:		
		rm -f *.o
		rm -f core
		rm -f *%
		rm -f a.out

#-------------------------------------------
# extract all programs. execute the xxxxxx file


extract_all:	extract_all.pl
		/bin/rm -f xxxxxx
		( cd RCS; /bin/ls *,v ) | extract_all.pl > xxxxxx
		csh -v xxxxxx


#include Makefile.Don
