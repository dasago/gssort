
/*--------------------------*/
/* user initialization code */
/* (may be empty..........) */
/*--------------------------*/
#if(1)
{
  HL = new HitList(200);       // A HitList Object for the hits in the Silicon detectors.
  GS = new GSHitList(200);   // A HitList Object for the hits in the Germanium detectors.
  S2 = new CySD("/home/dasago/Dropbox/Experiments/GSFMA339_12C+12C/Calibration/s2.control");
  S1I = new CySD("/home/dasago/Dropbox/Experiments/GSFMA339_12C+12C/Calibration/s1i.control");
  S1II = new CySD("s1ii",7.8, 2.4, 4.8, 16, 16);
  

  // DSG: Getting the run number to save it as a leaf in the TTree.  This number is useful when
  // analyzing merged files.
  size_t pos = gFileName.find(".dat.", 0);
  string StrRN = gFileName.substr(pos-3, 3);
  RunNum = atoi(StrRN.c_str());

  // DSG_tree: Data tree and branch definition.
  RawTree = new TTree("rawt","Raw data");
  RawTree->Branch("run",     &RunNum,      "run/I");
  RawTree->GetBranch("run")->SetCompressionSettings(205);
  RawTree->Branch("evtime",  &EvTime,      "evtime/D");
  RawTree->GetBranch("evtime")->SetCompressionSettings(205);
  RawTree->Branch("rftime",  &RFTime,      "rft/I");
  RawTree->GetBranch("rftime")->SetCompressionSettings(205);
  RawTree->Branch("cphits",  &HL->NumHits, "cphits/I");
  RawTree->GetBranch("cphits")->SetCompressionSettings(205);
  RawTree->Branch("mod",     HL->mod,      "mod[cphits]/I");
  RawTree->GetBranch("mod")->SetCompressionSettings(205);
  RawTree->Branch("ch",      HL->ch,       "ch[cphits]/I");
  RawTree->GetBranch("ch")->SetCompressionSettings(205);
  RawTree->Branch("val",     HL->val,      "val[cphits]/I");
  RawTree->GetBranch("val")->SetCompressionSettings(205);
  RawTree->Branch("gshits",  &GS->NumHits, "gshits/I");
  RawTree->GetBranch("gshits")->SetCompressionSettings(205);
  RawTree->Branch("geid",    GS->id,       "id[gshits]/I");
  RawTree->GetBranch("geid")->SetCompressionSettings(205);
  RawTree->Branch("ge",      GS->ge,       "ge[gshits]/F");
  RawTree->GetBranch("ge")->SetCompressionSettings(205);
  RawTree->Branch("gth",     GS->gth,      "gth[gshits]/F");
  RawTree->GetBranch("gth")->SetCompressionSettings(205);
  RawTree->Branch("gt",      GS->gt,       "gt[gshits]/F");
  RawTree->GetBranch("gt")->SetCompressionSettings(205);
  // The following line writes the branches data every 100 MB,
  // preventing your computer's RAM to get used 100%.
  // RawTree->SetAutoFlush(100000 /*Bytes*/);
  
  DetTree = new TTree("dett","Detectors' data");
  // S2
  // azimuthal branch
  DetTree->Branch("s2.ahits", &S2->ahits, "s2.ahits/I");
  DetTree->GetBranch("s2.ahits")->SetCompressionSettings(205);
  DetTree->Branch("s2.ach",   S2->ach,   "s2.ach[s2.ahits]/I");
  DetTree->GetBranch("s2.ach")->SetCompressionSettings(205);
  DetTree->Branch("s2.aval",  S2->aval,  "s2.aval[s2.ahits]/I");
  DetTree->GetBranch("s2.aval")->SetCompressionSettings(205);
  // polar branch
  DetTree->Branch("s2.phits", &S2->phits, "s2.phits/I");
  DetTree->GetBranch("s2.phits")->SetCompressionSettings(205);
  DetTree->Branch("s2.pch",   S2->pch,   "s2.pch[s2.phits]/I");
  DetTree->GetBranch("s2.pch")->SetCompressionSettings(205);
  DetTree->Branch("s2.pval",  S2->pval,  "s2.pval[s2.phits]/I");
  DetTree->GetBranch("s2.pval")->SetCompressionSettings(205);
  // S1I
  // azimuthal branch
  DetTree->Branch("s1i.ahits", &S1I->ahits, "s1i.ahits/I");
  DetTree->GetBranch("s1i.ahits")->SetCompressionSettings(205);
  DetTree->Branch("s1i.ach",   S1I->ach,   "s1i.ach[s1i.ahits]/I");
  DetTree->GetBranch("s1i.ach")->SetCompressionSettings(205);
  DetTree->Branch("s1i.aval",  S1I->aval,  "s1i.aval[s1i.ahits]/I");
  DetTree->GetBranch("s1i.aval")->SetCompressionSettings(205);
  // polar branch
  DetTree->Branch("s1i.phits", &S1I->phits, "s1i.phits/I");
  DetTree->GetBranch("s1i.phits")->SetCompressionSettings(205);
  DetTree->Branch("s1i.pch",   S1I->pch,   "s1i.pch[s1i.phits]/I");
  DetTree->GetBranch("s1i.pch")->SetCompressionSettings(205);
  DetTree->Branch("s1i.pval",  S1I->pval,  "s1i.pval[s1i.phits]/I");
  DetTree->GetBranch("s1i.pval")->SetCompressionSettings(205);
  // S1II
  // azimuthal branch
  DetTree->Branch("s1ii.ahits", &S1II->ahits, "s1ii.ahits/I");
  DetTree->GetBranch("s1ii.ahits")->SetCompressionSettings(205);
  DetTree->Branch("s1ii.ach",   S1II->ach,    "s1ii.ach[s1ii.ahits]/I");
  DetTree->GetBranch("s1ii.ach")->SetCompressionSettings(205);
  DetTree->Branch("s1ii.aval",  S1II->aval,   "s1ii.aval[s1ii.ahits]/I");
  DetTree->GetBranch("s1ii.aval")->SetCompressionSettings(205);
  // polar branch
  DetTree->Branch("s1ii.phits", &S1II->phits, "s1ii.phits/I");
  DetTree->GetBranch("s1ii.phits")->SetCompressionSettings(205);
  DetTree->Branch("s1ii.pch",   S1II->pch,    "s1ii.pch[s1ii.phits]/I");
  DetTree->GetBranch("s1ii.pch")->SetCompressionSettings(205);
  DetTree->Branch("s1ii.pval",  S1II->pval,   "s1ii.pval[s1ii.phits]/I");
  DetTree->GetBranch("s1ii.pval")->SetCompressionSettings(205);
  // Monitor top
  /* DetTree->Branch("mont",     &MonTop,     "mont/I"); */
  /* DetTree->GetBranch("mont")->SetCompressionSettings(205); */
  // Beam current integrator (BCI)
  DetTree->Branch("bcihits", &bcihits, "bcihits/I");
  DetTree->GetBranch("bcihits")->SetCompressionSettings(205);
  bcival = new int[max_bcihits];
  bcihits = max_bcihits;
  DetTree->Branch("bcival",  bcival,   "bcival[bcihits]/I");
  DetTree->GetBranch("bcival")->SetCompressionSettings(205);
  // The following line writes the branches data every 100 MB,
  // preventing your computer's RAM to get used 100%.
  // DetTree->SetAutoFlush(100000 /*Bytes*/);


  // We want the detector tree to have access to the branches in the
  // raw tree. To accomplish this we use the AddFriend method
  DetTree->AddFriend(RawTree->GetName());
  DetTree->Print();

  // Find the ID's of the DSSD channels and monitor detector.
  for(i=0; i<16; i++){
    sprintf(str1, "dssd1_%i", i);
    PType = FindPEvMatNo(str1, &id_dssd1[i]);
    sprintf(str1, "dssd2_%i", i);
    PType = FindPEvMatNo(str1, &id_dssd2[i]);
    sprintf(str1, "dssd3_%i", i);
    PType = FindPEvMatNo(str1, &id_dssd3[i]);
    sprintf(str1, "dssd4_%i", i);
    PType = FindPEvMatNo(str1, &id_dssd4[i]);
    sprintf(str1, "dssd5_%i", i);
    PType = FindPEvMatNo(str1, &id_dssd5[i]);
    sprintf(str1, "dssd6_%i", i);
    PType = FindPEvMatNo(str1, &id_dssd6[i]);
  }
  PType = FindPEvMatNo("mon1", &id_mon1);
  PType = FindPEvMatNo("mon2", &id_mon2);
  PType = FindPEvMatNo("beamintegral", &id_beamint);

  for(i=0; i<6; i++){
    sprintf(str1, "tdssd%i", i);
    //printf("tdssd_%i", i);
    PType = FindPEvMatNo(str1, &id_tdssd[i]);
  }

  // Make a bunch of hitograms.  The mkTH2F does not have a reason to exist.  It would be better to
  // use arrays of pointers (TH2F**) instead of this large set of histrogram pointer.

  sprintf(str1, "current");
  sprintf(str2, "beam current");
  h_current = mkTH1D(str1, str2, LENEVTIME, 0, LENEVTIME);
  sprintf(str1, "Seconds");
  h_current->SetXTitle(str1);
  h_current->SetYTitle("Beam Current");

 
  sprintf(str1,"gamgam");
  gg = mkTH2F(str1,str1,8000,0,8000,8000,0,8000);  
}
#endif
