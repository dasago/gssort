
/*-----------------------*/
/* user declaration code */
/* (may be empty.......) */
/*-----------------------*/
#if(1)
int FindPEvMatNo(char *, int *);

// DSG_tree:  The following 5 lines are related to the creation of a TTree.
TTree* RawTree;   // TTree pointer for the raw data structure.
TTree* DetTree;   // TTree pointer for the detector data structure.
int RunNum;       // The run number (useful when a file contains data from multiple runs)
int RFTime;
HitList* HL;      // A HitList Object for the hits in the Silicon detectors.
GSHitList* GS;    // A HitList Object for the hits in the Germanium detectors.
// Cylindrical Silicon Detectors
CySD* S2;
CySD* S1I;
CySD* S1II;
// Beam current integrator
const int max_bcihits = 1000;
int bcihits;
int* bcival;

int PType;
int id_dssd1[16],id_dssd2[16],id_dssd3[16],id_dssd4[16],id_dssd5[16],id_dssd6[16];
int id_tdssd[6];
int id_mon1;
int id_mon2;
int id_beamint;
int jj,kk,dtjk;

TH1D *h_current;
double CurrentTime0, CurrentTime;
TH2F *gg;

#endif
