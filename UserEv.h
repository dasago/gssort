
/*-----------------*/
/* user event code */
/* (may be empty.) */
/*-----------------*/
#if(1)
{
  if (CurEvNo<100) {
    CurrentTime0 = (double)hdr.ttL + 65536.0 * (double)hdr.ttM + 2147483648.0 * (double)hdr.ttH;
    CurrentTime0 /= 1000000.0;	/* now seconds */
  }

  // DSG_tree: Filling the data tree
  RFTime = PEv[TAC2].val;
  HL->Reset();
  GS->Reset();
  S2->Reset();
  S1I->Reset();
  S1II->Reset();
  for (int h=0; h<bcihits; h++)
    bcival[h] = 0;
  bcihits = 0;
  
  // Gammasphere hit
  for(int gsh=0; gsh<hdr.len_clean; gsh++) { 
    if (ev[gsh].ehi>10 && ev[gsh].tge>0 && GS->NumHits<GS->MaxHits) {
      int h = GS->NumHits;
      GS->id[h] = ev[gsh].id;            // Germanium detector ID
      GS->ge[h] = ev[gsh].ehi;           // High-resolution gamma energy
      GS->gth[h] = angle[ev[gsh].id];    // Polar angle of germanium detector 
      GS->gt[h] = ev[gsh].tge;           // Germanium detector time
      GS->NumHits++;
    }        
  }    
  // DSSD1 data (quick-and-dirty way of doing it)
  for (int ch=0; ch<16; ch++) {
    if (PEv[id_dssd1[ch]].val>0) {
      int h = HL->NumHits;
      int val = PEv[id_dssd1[ch]].val;
      HL->mod[h] = 1;
      HL->ch[h] = ch;
      HL->val[h] = val;
      HL->NumHits++;  
      S2->AddPolarSignal(val, ch);
    }
  }
  // DSSD2 data (quick-and-dirty way of doing it)
  for (int ch=0; ch<16; ch++) {
    if (PEv[id_dssd2[ch]].val>0) {
      int h = HL->NumHits;
      int val = PEv[id_dssd2[ch]].val;
      HL->mod[h] = 2;
      HL->ch[h] = ch;
      HL->val[h] = val;
      HL->NumHits++; 
      S2->AddAzimuthalSignal(val, ch);
    }
  }
  // DSSD3 data (quick-and-dirty way of doing it)
  for (int ch=0; ch<16; ch++) {
    if (PEv[id_dssd3[ch]].val>0) {
      int h = HL->NumHits;
      int val = PEv[id_dssd3[ch]].val;
      HL->mod[h] = 3;
      HL->ch[h] = ch;
      HL->val[h] = val;
      HL->NumHits++;
      S1I->AddPolarSignal(val, ch);
    }
  }
  // DSSD4 data (quick-and-dirty way of doing it)
  for (int ch=0; ch<16; ch++) {
    if (PEv[id_dssd4[ch]].val>0) {
      int h = HL->NumHits; 
      int val = PEv[id_dssd4[ch]].val;
      HL->mod[h] = 4;
      HL->ch[h] = i;
      HL->val[h] = val;
      HL->NumHits++;
      S1I->AddAzimuthalSignal(val, ch);
    }
  }
  // DSSD5 data (quick-and-dirty way of doing it)
  for (int ch=0; ch<16; ch++) {
    if (PEv[id_dssd5[ch]].val>0) {
      int h = HL->NumHits;
      int val = PEv[id_dssd5[ch]].val;
      HL->mod[h] = 5;
      HL->ch[h] = ch;
      HL->val[h] = val;
      HL->NumHits++;    
      S1II->AddPolarSignal(val, ch);
    }
  }
  // DSSD6 data (quick-and-dirty way of doing it)
  for (int ch=0; ch<16; ch++) {
    if (PEv[id_dssd6[ch]].val>0) {
      int h = HL->NumHits;
      int val = PEv[id_dssd6[ch]].val;
      HL->mod[h] = 6;
      HL->ch[h] = ch;
      HL->val[h] = val;
      HL->NumHits++;
      S1II->AddAzimuthalSignal(val, ch);
    }
  }
  // Monitor Top data (quick-and-dirty way of doing it)
  if (PEv[id_mon1].val>0) {
    int h = HL->NumHits;
    HL->mod[h] = 7;
    HL->ch[h] = 0;
    HL->val[h] = PEv[id_mon1].val;
    HL->NumHits++;
  }
  // Monitor Bottom data (quick-and-dirty way of doing it)
  if (PEv[id_mon2].val>0) {
    int h = HL->NumHits;
    HL->mod[h] = 7;
    HL->ch[h] = 1;
    HL->val[h] = PEv[id_mon2].val;
    HL->NumHits++;
  }
  // Beam current integrator data (quick-and-dirty way of doing it)
  if (PEv[id_beamint].val>0) {
    int h = HL->NumHits;
    HL->mod[h] = 7;
    HL->ch[h] = 2;
    HL->val[h] = PEv[id_beamint].val;
    bcival[bcihits] = PEv[id_beamint].val;
    bcihits++;
    HL->NumHits++;
  }
  
  // DSG_tree: Sort hits (decreasing order in energy, i.e. large signals first) and fill the TTree.
  if (HL->NumHits>0 || GS->NumHits>0) {
    HL->SortHits();
    
    S2->SortHits();
    S1I->SortHits();
    S1II->SortHits();

    RawTree->Fill();
    DetTree->Fill();

    /* if(RawTree->GetEntries()%100000==0) { */
    /*   RawTree->FlushBaskets(); */
    /*   DetTree->FlushBaskets(); */
    /*   //cout << "\nBytes written to file: " << BytesWritten << endl; */
    /*   //tree->Print(); */
    /* } */
    
  }

  // Fill gamma-gamma matrix
  for (jj=0; jj<hdr.len_clean; jj++) {
    for(kk=jj+1; kk<hdr.len_clean; kk++) {
      dtjk=ev[jj].tge-ev[kk].tge;
      if(dtjk>=-60 && dtjk<=60){
	gg->Fill(ev[jj].ehi,ev[kk].ehi);
	gg->Fill(ev[kk].ehi,ev[jj].ehi);
      }
    }
  }
}
#endif
